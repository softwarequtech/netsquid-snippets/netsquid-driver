from abc import ABCMeta, abstractmethod
from dataclasses import dataclass

from netsquid.protocols.serviceprotocol import ServiceProtocol


@dataclass
class BaseEntanglementAgreement:
    """Base dataclass for entanglement agreements requests and results.

    Parameters
    ----------
    remote_node_name : str
        Name of the remote node that the agreement change relates to.
    connection_id : int
        ID of the entangling connection about which agreement change relates to.
        Useful when there are multiple ways of generating entanglement between two nodes.

    """
    remote_node_name: str
    connection_id: int = 0


@dataclass
class ReqEntanglementAgreement(BaseEntanglementAgreement):
    """Request to reach agreement with a remote node to generate entanglement."""


@dataclass
class ReqEntanglementAgreementAbort(BaseEntanglementAgreement):
    """Request to abort reaching agreement with a remote node to generate entanglement."""


@dataclass
class ResEntanglementAgreementRejected(BaseEntanglementAgreement):
    """Response indicating that agreement for entanglement generation cannot currently be reached."""


@dataclass
class ResEntanglementAgreementReached(BaseEntanglementAgreement):
    """Response indicating that agreement for entanglement generation has been reached."""


signal_entanglement_agreement_status_change = "signal_entanglement_agreement_status_change"


class EntanglementAgreementService(ServiceProtocol, metaclass=ABCMeta):
    """Service for reaching agreement about entanglement generation with other nodes.

    If a node wants to generate entanglement with another node, the nodes must reach agreement and synchronize
    their attempts before they can start.
    This is the responsibility of :class:`EntanglementAgreementService`.

    To request agreement, a :class:`ReqEntanglementAgreement`
    should be issued at this service.
    The service will respond to this by either sending a
    :class:`ResEntanglementAgreementRejected` response if agreement cannot be reached
    right now (thereby discarding the request), or by sending a
    :class:`ResEntanglementAgreementReached` if agreement is reached and
    entanglement generation can (and should, in case synchronization is important) be commenced right away.

    If agreement has been requested but is no longer required, a previous request can be cancelled by issuing a
    :class:`ReqEntanglementAgreementAbort`.

    In case there is some change in the status of :class:`EntanglementAgreementService`, it should send the signal
    `signal_entanglement_agreement_status_change`.
    The purpose of this, is that a request that was previously rejected, might suddenly be acceptable after the status
    change.
    Therefore, notifying other services of such changes is important, so that they can reissue their rejected requests
    if they want to.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    name : str, optional

    """
    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self.register_request(req_type=ReqEntanglementAgreement, handler=self.try_to_reach_agreement)
        self.register_request(req_type=ReqEntanglementAgreementAbort, handler=self.abort)
        self.register_response(res_type=ResEntanglementAgreementRejected)
        self.register_response(res_type=ResEntanglementAgreementReached)
        self.add_signal(signal_entanglement_agreement_status_change)

    @abstractmethod
    def try_to_reach_agreement(self, req):
        """Handle entanglement-agreement request, :class:`ReqEntanglementAgreement`.

        Reach agreement with another node (specified in the request) about whether entanglement can be generated.
        Must call `EntanglementAgreementService.send_response(res)` where res is...
        :class:`ResEntanglementAgreementRejected` if entanglement cannot be generated, or
        :class:`ResEntanglementAgreementReached` if there is agreement and
        entanglement generation can (or should) start right now.

        If it is not yet clear whether entanglement can be generated or not,
        the sending of a response can be held off until a decision has been made, i.e. until either
        agreement has been reached or it is concluded that agreement will not be reached (now).

        Parameters
        ----------
        req : :class:`ReqEntanglementAgreement`
            Request holding requirements on the agreement that must be reached.

        """
        assert isinstance(req, ReqEntanglementAgreement)

    @abstractmethod
    def abort(self, req):
        """Abort the execution of a previous request for agreement.

        Parameters
        ----------
        req : :class:`ReqEntanglementAgreementAbort`
            Request specifying which attempt at agreement should be aborted.

        """
        assert isinstance(req, ReqEntanglementAgreementAbort)

    def _send_update_signal(self):
        """Send signal to indicate a status change that could make previously unacceptable requests acceptable.

        This method should be called by implementations of :class:`EntanglementAgreementService` whenever there is some
        change in the state of the protocol that might lead to the acceptance of requests for agreement that were
        previously rejected.

        Other protocols can pick up this signal by listening for
        `signal_entanglement_agreement_status_change`.
        As a response to this signal, they can try to reissue their previously rejected requests.

        """

        self.send_signal(signal_entanglement_agreement_status_change)
