import unittest

from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes.node import Node
from netsquid_driver.memory_manager_service import QuantumMemoryManager, ReqFreeMemory, ReqMove


class MockMemoryManager(QuantumMemoryManager):
    def __init__(self, node, size=None):
        super().__init__(node=node,
                         size=size)
        self.performed_move = False
        self.freed_up_position = False

    def move(self, from_memory_position_id, to_memory_position_id=None):
        self.performed_move = True

    def free_position(self, memory_position_id):
        self.freed_up_position = True


class TestMemoryManagerService(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        qprocessor = QuantumProcessor(name="test_processor",
                                      num_positions=3)
        cls.node = Node(name="test_node",
                        qmemory=qprocessor)

    def setUp(self):
        self.size = 2
        self.memory_manager = MockMemoryManager(node=self.node,
                                                size=self.size)

    def test_init(self):
        assert self.memory_manager.size == 2

    def test_req_move(self):
        req = ReqMove(from_memory_position_id=0)
        self.memory_manager.put(req)
        assert self.memory_manager.performed_move

    def test_req_free_memory(self):
        req = ReqFreeMemory()
        self.memory_manager.put(req)
        assert self.memory_manager.freed_up_position
