from __future__ import annotations

import dataclasses
import logging
from copy import deepcopy, copy
from typing import List, Dict, Any, Type

from netsquid.components import Message, Port
from netsquid.protocols import NodeProtocol, ServiceProtocol

logger = logging.getLogger(__name__)


@dataclasses.dataclass
class RemoteServiceRequest:
    """Request for :class:`ClassicalRoutingService` for sending a request to a service on another node."""
    request: Any
    """The request to send."""
    service: Type[ServiceProtocol]
    """The class type of the service that the request will be sent to."""
    targets: List[str]
    """The node names where the request is sent to."""
    origin: str
    """The node name of the node that created this remote service request."""
    _forward_counter = 1000
    """Counter that tracks how often this message is allowed to be forwarded.
    Each forwarding will subtract 1 and an error is raised once it reaches zero.
    The purpose is to prevent loops."""
    _is_broadcast = False
    """Flag to indicate if the message is being sent to all nodes in the network."""

    def __str__(self) -> str:
        return f"Message, Origin {self.origin}, Targets = {self.targets}" \
               f", payload =\n\t{self.request}, service = {self.service.__name__}"


class ClassicalRoutingService(ServiceProtocol):
    """A service for sending requests to services running on other nodes.

    This service provides a network/transport layer for communication to services by routing requests across the network
    and delivering them to the intended service.

    This service is intended to run on all nodes in a network with routing tables set up appropriately
    to facilitate routing requests to any node in the network.

    Parameters
    ----------
    node : :obj:`~netsquid.nodes.node.Node`
        Node at which this ClassicalRoutingService resides.
    local_routing_table: dict
        Dictionary that maps from a remote node name to a port.
        This is used to rout messages to their intended destination.
        Specifically this dictionary is used to forward messages with the given destination name
        to a port given by this dictionary.
    """

    def __init__(self, node, local_routing_table: Dict[str, Port], name=None):
        super().__init__(node, name)
        self.routing_table: Dict[str, Port] = local_routing_table
        self.register_request(RemoteServiceRequest, self._handle_remote_service_req)
        self.logger = logger.getChild(self.node.name)
        self._listening_ports: List[Port] = list(local_routing_table.values())
        for listening_port in self._listening_ports:
            assert listening_port.name in self.node.ports

    def broadcast(self, message: RemoteServiceRequest):
        """Send a request to all known nodes in the network.

        This will set the targets in :class:`RemoteServiceRequest` to all known nodes in the network.
        """
        # deepcopy to prevent modification of RemoteServiceRequest after request has been made
        message = deepcopy(message)
        message.targets = [node_name for node_name in self.routing_table.keys() if node_name != self.node.name]
        message._is_broadcast = True
        self.logger.info(f"Broadcasting message {message}")
        self.put(message)

    def _handle_remote_service_req(self, remote_service_req: RemoteServiceRequest):
        # deepcopy to prevent modification of RemoteServiceRequest after request has been made
        req = deepcopy(remote_service_req)
        self._forward_to_local_service(req)
        self._forward_to_remote_nodes(req)

    def _forward_to_remote_nodes(self, req: RemoteServiceRequest):
        """Forward the :class:`RemoteServiceRequest` to the relevant connected nodes."""
        targets_per_port = self._find_targets_per_port(req.targets)
        req._forward_counter -= 1
        if req._forward_counter < 0:
            raise RuntimeError(f"Message has exceeded maximum forwards, likely a loop is present.\n{req}")

        for port, targets in targets_per_port.items():
            message = copy(req)
            message.targets = targets
            port.tx_output(message)
            self.logger.debug(f"Forwarding message to port {port.name}, message:\n\t{message}")

    def start(self):
        self._register_subprotocols()
        super().start()

    def _register_subprotocols(self):
        """This function initialises `ListeningSubProtocol`s for all ports of given node.

        For each port, it checks if there is already a subprotocol listening. If not, one is created.
        """
        for port in self._listening_ports:
            if self._subprotocol_name(port.name) not in self._subprotocols:
                subprotocol = self.ListeningSubProtocol(node=self.node, port_name=port.name,
                                                        superprotocol=self)
                self.add_subprotocol(subprotocol=subprotocol, name=self._subprotocol_name(port.name))

    @staticmethod
    def _subprotocol_name(port_name) -> str:
        """Name of the `ListeningSubProtocol` listening on a specific port."""
        return f"ListeningSubProtocol_{port_name}"

    def _find_targets_per_port(self, targets: List[str]) -> Dict[Port, List[str]]:
        """
        Groups target node names by the network port through which they are reachable.

        Given a list of target node names, this method uses the routing table to determine
        the appropriate network port for each target. Targets are organized into a dictionary
        where the keys are the network ports, and the values are lists of target nodes reachable via those ports.
        """
        targets_per_port = {}
        for target in targets:
            if target == self.node.name:
                continue
            if target not in self.routing_table.keys():
                self.logger.critical(f"No entry in forwarding table for {target} at node {self.node.name}")
                continue
            port = self.routing_table[target]
            if port not in targets_per_port.keys():
                targets_per_port[port] = []
            targets_per_port[port].append(target)

        return targets_per_port

    def _forward_to_local_service(self, remote_service_req: RemoteServiceRequest):
        """Forward the request of :class:`RemoteServiceRequest` to the relevant service
        if the request is intended for this node."""
        if self.node.name in remote_service_req.targets:
            if remote_service_req.service not in self.node.driver.services:
                raise KeyError(f"Could not find service: {remote_service_req.service} on node: {self.node.name}")

            # Log _is_broadcast requests at a lower level, as there will be many of these
            if not remote_service_req._is_broadcast:
                self.logger.info(f"Received RemoteServiceRequest for this node:\n\t{remote_service_req}")
            else:
                self.logger.debug(f"Received broadcast RemoteServiceRequest for this node:\n\t{remote_service_req}")

            # Deepcopy request to prevent passing objects, like shared array, in a request across multiple nodes
            request = deepcopy(remote_service_req.request)
            self.node.driver.services[remote_service_req.service].put(request)

    class ListeningSubProtocol(NodeProtocol):
        """This sub protocol listens to incoming messages on a port and will both forward
        RemoteServiceRequests to remote nodes and the relevant service on this node."""

        def __init__(self, node, port_name, superprotocol: ClassicalRoutingService):
            """
            Parameters
            ----------
            node : :obj:`~netsquid.nodes.node.Node`
                Parent node at which the port of this protocol resides.
            port_name : str
                Name of the port for which is this ListeningPortProtocol initialised.
            superprotocol: ClassicalRoutingService
                The superprotocol.
            """
            super().__init__(node=node, name=f"ListeningPort_{port_name}")

            self._port_name = port_name
            self._superprotocol = superprotocol
            self._port = node.ports[self._port_name]

        def run(self):
            """Listens for incoming messages on this port and handle any incoming messages."""
            while True:
                yield self.await_port_input(self._port)
                message: Message = self._port.rx_input()
                for item in message.items:
                    self._handle_message(item)

        def _handle_message(self, message):
            """Handles messages of type :class:`RemoteServiceRequest`.

            Will both forward the :class:`RemoteServiceRequest` to remote nodes and to local services.
            """
            self._superprotocol.logger.debug(f"Received message:\n\t{message}")

            if not isinstance(message, RemoteServiceRequest):
                self._superprotocol.logger.warning(f"Received message that can't be processed of "
                                                   f"type: {type(message)}. The message is:\n\t{message}")
                return

            self._superprotocol._forward_to_remote_nodes(message)
            self._superprotocol._forward_to_local_service(message)
