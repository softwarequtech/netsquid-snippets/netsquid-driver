from __future__ import annotations

from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Generator

from netsquid.protocols import ServiceProtocol
from pydynaa import EventExpression


@dataclass
class SocketAddress:
    """Class representing a socket address with a node name and port name."""
    node_name: str
    """The name of the node."""
    port_name: str
    """The name of the port."""


class ClassicalSocket(metaclass=ABCMeta):
    """Classical socket, used for simulating classical data exchange."""

    @abstractmethod
    def bind(self, port_name: str, remote_node_name: str = None):
        """Binds the socket to a port and optionally to a remote node.

        :param port_name: The name of the port to bind to.
        :param remote_node_name: The name of the remote node to bind to.
         If not specified or set to None, it will receive from all remote nodes.
        """
        pass

    @abstractmethod
    def connect(self, remote_port_name: str, remote_node_name: str):
        """Connects the socket to a remote port and node.

        :param remote_port_name: The name of the remote port to connect to.
        :param remote_node_name: The name of the remote node to connect to.
        """
        pass

    @abstractmethod
    def send(self, msg: str):
        """Sends a string message to a connected remote socket.

        :param msg: The message to send.
        """
        pass

    @abstractmethod
    def send_to(self, msg: str, remote_port_name: str, remote_node_name: str) -> None:
        """Sends a message to the remote port and node.

        :param msg: The message to send.
        :param remote_port_name: The name of the remote port to send to.
        :param remote_node_name: The name of the remote node to send to.
        """
        pass

    @abstractmethod
    def recv(self) -> Generator[EventExpression, None, str]:
        """Wait and receives a string message.

        :return: A generator that yields an event expression and returns a string message.
        """
        pass

    @abstractmethod
    def recv_from(self) -> Generator[EventExpression, None, (str, SocketAddress)]:
        """Wait and receive a string message and return both the message and the address it was sent from.

        :return: A generator that yields an event expression and returns a tuple containing the string message
         and the sender's socket address.
        """
        pass

    @abstractmethod
    def close(self):
        """Closes the socket."""
        pass


class ClassicalSocketService(ServiceProtocol, metaclass=ABCMeta):
    """Service that supports classical sockets."""

    @abstractmethod
    def create_socket(self, **kwargs) -> ClassicalSocket:
        """Creates a new classical socket.

        :return: An instance of a ClassicalSocket.
        """
        pass
