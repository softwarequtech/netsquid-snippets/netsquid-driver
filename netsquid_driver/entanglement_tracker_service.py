from abc import ABCMeta, abstractmethod
from copy import deepcopy
from dataclasses import dataclass
from enum import Enum
from typing import FrozenSet, Optional, Union

import netsquid as ns
from netsquid.protocols.serviceprotocol import ServiceProtocol
from netsquid.qubits.ketstates import BellIndex


@dataclass(frozen=True)
class LinkIdentifier:
    """Identifier for entangled states generated on elementary links.

    Parameters
    ----------
    node_ids : set of int
        :prop:`netsquid.nodes.node.Node.ID` of the nodes that generated the entangled pair
    pair_identifier : int
        Some identifier of the entangled state that is unique for this specific value of `node_ids`.

    """
    node_ids: FrozenSet[int]
    pair_identifier: int

    def __post_init__(self):
        if not isinstance(self.node_ids, frozenset):
            raise TypeError(f"{self.node_ids} is not a frozenset. "
                            f"The node_ids parameter must be a frozenset to make hashing and comparing"
                            f"LinkIdentifiers possible.")


class LinkStatus(Enum):
    """Status of an elementary-link entangled state."""
    AVAILABLE = 1  # the local qubit that was created as part of the elementary-link entangled state is available
    MEASURED = 2  # the qubit has been measured (e.g. as part of entanglement swapping)
    DISCARDED = 3  # the qubit has been discarded (e.g. because a cutoff timer is used)


@dataclass
class ReqNewLinkUpdate:
    """Request to update knowledge with information about a newly-generated elementary-link entangled state.

    Parameters
    ----------
    link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the newly-generated elementary link.
    bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
        Index of the Bell state that was generated.
        If None, the Bell index is not tracked.
    times_of_birth : dict or float or None
        Times at which the qubits of the elementary-link entangled state indicated by `link_identifier`
        came into existence.
        Dictionary should have the :prop:`netsquid.nodes.node.Node.ID` (int) as keys
        (to refer to specific qubits in the elementary-link entangled state),
        and float or None as values (the time at which the corresponding qubit came into existence).
        If float or None is passed instead of a dictionary, this is used as the time of birth for all qubits
        in the elementary-link entangled state.
        When None is used as a time of birth, the time of birth is not tracked.

    """
    link_identifier: LinkIdentifier
    bell_index: Optional[BellIndex] = None
    times_of_birth: Union[Optional[float], dict] = None


@dataclass
class ReqSwapUpdate:
    """Request to update knowledge with information about an entanglement swap.

    Parameters
    ----------
    link_identifier_1 : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of one of the two elementary-link states that were swapped.
    link_identifier_2 : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the other elementary-link state that was swapped.
    bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
        Bell index of the outcome of the Bell-state measurement.
        If None, the Bell index is not tracked.
    time_of_swap : float or None
        Time at which the swap was performed.
        If None, the time at which this request is issued is used instead.

    """
    link_identifier_1: LinkIdentifier
    link_identifier_2: LinkIdentifier
    bell_index: Optional[BellIndex] = None
    time_of_swap: Optional[float] = None


@dataclass
class ReqDiscardUpdate:
    """Request to update knowledge with information about the discard of an entangled qubit.

    Parameters
    ----------
    link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the elementary-link entangled state as part of which the discarded qubit was created.
    node_id : int
        :prop:`netsquid.nodes.node.Node.ID` of the node at which the discard has taken place.
    time_of_discard : float or None
        Time at which the link was discarded.
        If None, the time at which this request is issued is used instead.

    """
    link_identifier: LinkIdentifier
    node_id: int
    time_of_discard: Optional[float] = None


@dataclass
class ReqMeasureUpdate:
    """Request to update knowledge with information about the measurement of an entangled qubit.

    Parameters
    ----------
    link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the elementary-link entangled state as part of which the measured qubit was created.
    node_id : int
        :prop:`netsquid.nodes.node.Node.ID` of the node at which the measurement has taken place.
    time_of_measurement : float or None
        Time at which the qubit was measured.
        If None, the time at which this request is issued is used instead.

    """
    link_identifier: LinkIdentifier
    node_id: int
    time_of_measurement: Optional[float] = None


@dataclass
class ResNewLocalLink:
    """Response to indicate that a new local qubit has been created that is part of an elementary-link entangled state.

    Parameters
    ----------
    link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the new elementary-link entangled state that the qubit belongs to.
    memory_position: int or None
        Memory position at which the entangled qubit is stored.

    """
    link_identifier: LinkIdentifier
    memory_position: Optional[int] = None


@dataclass
class ResLocalDiscard:
    """Response to indicate that a local qubit has been discarded that was part of an elementary-link entangled state.

    Parameters
    ----------
    link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the elementary-link entangled state that the qubit belonged to.
    memory_position: int
        Memory position at which the entangled qubit was stored.

    """
    link_identifier: LinkIdentifier
    memory_position: Optional[int] = None


@dataclass
class ResLocalMeasure:
    """Response to indicate that a local qubit has been measured that was part of an elementary-link entangled state.

    Parameters
    ----------
    link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the elementary-link entangled state that the qubit belonged to.
    memory_position: int or None
        Memory position at which the entangled qubit was stored.

    """
    link_identifier: LinkIdentifier
    memory_position: Optional[int] = None


class EntanglementInfo:
    """Holder for information about an entangled state shared across multiple nodes.

    This class is used for tracking entanglement in quantum networks.
    One instance of `EntanglementInfo` represents a single entangled state.
    It holds the following information:
    - Which elementary-link entangled states were used to create this entangled state.
    - Which of the qubits that were created as part of elementary-link entangled states have been measured out,
    and which are still entangled (it is assumed that all qubits that were not measured out all share a single
    entangled state).
    - Which Bell states the elementary-link entangled states were.
    - At which time each elementary-link entangled qubit was created.
    - At which time each elementary-link entangled qubit ceased to exist (if ever).

    The entangled state that is tracked by an `EntanglementInfo` can be shared across any number of nodes
    (i.e. it does not need to be a bipartite state).
    When an `EntanglementInfo` is initialized, it is "elementary", holding information about
    a single elementary-link entangled state (which is bipartite).
    `EntanglementInfo`s tracking larger entangled states can be created by merging `EntanglementInfo`s.
    This can be done using :meth:`entanglement_tracker_service.EntanglementInfo.merge`.
    After merging, an `EntanglementInfo` is obtained which assumes that all the qubits that were entangled in the
    two original `EntanglementInfo`s share one big entangled state.
    To remove qubits from the entangled state (to e.g. represent what happens during a Bell-state measurement),
    they can be "unentangled" using :meth:`entanglement_tracker_service.EntanglementInfo.unentangle_qubit`.
    A qubit that has been set to "unentangled", is assumed to have been unentangled in such a way that all other qubits
    in the state are still entangled with one another.

    Notes
    -----
    This class is meant to be subclassed in case a specific class of entangled states (such as Bell states)
    are tracked, or if additional information about entangled states needs to be tracked.
    To track additional information, the inner classes `_LinkData` and `_QubitData` can be overwritten
    (preferably by subclassing and adding any extra properties of links or qubits that need to be tracked).

    Parameters
    ----------
    link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the elementary-link entangled state for which entanglement information should be stored.
    bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
        Index indicating what Bell state the elementary-link entangled state is.
        If None, the bell index is not tracked.
    times_of_birth : dict or float or None
        Times at which the qubits of the elementary-link entangled state indicated by `link_identifier`
        came into existence.
        Dictionary should have the :prop:`netsquid.nodes.node.Node.ID` (int) as keys
        (to refer to specific qubits in the elementary-link entangled state),
        and float or None as values (the time at which the corresponding qubit came into existence).
        If float or None is passed instead of a dictionary, this is used as the time of birth for all qubits
        in the elementary-link entangled state.
        When None is used as a time of birth, the time of birth is not tracked.

    """

    @dataclass
    class _LinkData:
        """Holder for data to be stored about an elementary-link entangled state.

        Parameters
        ----------
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex
            Bell index indicating which Bell state the elementary-link entangled state is thought ot be.

        """
        bell_index: Optional[BellIndex] = None

    @dataclass
    class _QubitData:
        """Holder for data to be stored in `EntanglmentInfo` about one specific qubit.

        Parameters
        ----------
        entangled : bool
            True if the qubit is entangled with the other qubits tracked by this `EntanglementInfo`.
        time_of_birth : float or None
            Time at which the qubit came into existence.
            None if no time of birth is known.
        time_of_death : float or None
            Time at which the qubit ceased to exist.
            None if no time of death is known (and the qubit is still considered to exist).

        """
        entangled: bool = True
        time_of_birth: Optional[float] = None
        time_of_death: Optional[float] = None

    @dataclass(frozen=True)
    class _QubitIdentifier:
        """Identifier for a qubit that was created as part of an elementary-link entangled state.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state as part of which this qubit was created.
        node_id : int
            :prop:`netsquid.nodes.node.Node.ID` of the node at which this qubit was created.

        """
        link_identifier: LinkIdentifier
        node_id: int

        def __post_init__(self):
            if not isinstance(self.link_identifier, LinkIdentifier):
                raise TypeError(f"{self.link_identifier} is not a LinkIdentifier.")
            if self.node_id not in self.link_identifier.node_ids:
                raise ValueError(f"{self.node_id} is not one fo the node_ids of link_identifier.")

    def __init__(self, link_identifier, bell_index=None, times_of_birth=None):
        if not isinstance(link_identifier, LinkIdentifier):
            raise TypeError(f"link_identifier must be a LinkIdentifier, not {link_identifier}.")
        if not isinstance(times_of_birth, dict):
            times_of_birth = {node_id: times_of_birth for node_id in link_identifier.node_ids}
        if not set(times_of_birth.keys()) == link_identifier.node_ids:
            raise ValueError("The keys of times_of_birth must be the node_ids of link_identifier.")
        self._link_identifiers = {link_identifier}
        self._link_data = {link_identifier: self._LinkData(bell_index=bell_index)}
        self._qubit_data = {}
        for node_id in link_identifier.node_ids:
            qubit_identifier = self._QubitIdentifier(link_identifier, node_id)
            qubit_data = self._QubitData()
            self._qubit_data[qubit_identifier] = qubit_data
        for node_id, time_of_birth in times_of_birth.items():
            if time_of_birth is not None:
                self.set_time_of_birth(node_id=node_id, link_identifier=link_identifier, time_of_birth=time_of_birth)
        self._intact = True

    @property
    def elementary(self):
        """Indicates whether this entangled state is elementary.

        An entangled state is considered elementary if it was generated using a single elementary link,
        i.e. no nodes except for the two sharing the state were involved in making the state.

        Returns
        -------
        bool
            True if the entangled state is elementary.

        """
        return len(self.elementary_links) == 1

    @property
    def intact(self):
        """Indicates whether the entangled state is intact.

        If an entangled state is not intact, this means that for some reason the entangled state has been destroyed,
        and the qubits are no longer entangled.
        A reason for setting `EntanglementInfo.intact = False` can e.g. be that one of the entangled qubits of the state
        was discarded or lost.

        Returns
        -------
        bool
            True if the entangled state is intact.

        """
        return self._intact

    @intact.setter
    def intact(self, intact):
        """Indicates whether the entangled state is intact.

        If an entangled state is not intact, this means that for some reason the entangled state has been destroyed,
        and the qubits are no longer entangled.
        A reason for setting `EntanglementInfo.intact = False` can e.g. be that one of the entangled qubits of the state
        was discarded or lost.

        Parameters
        -------
        intact : bool
            True if the entangled state is intact.

        """
        self._intact = bool(intact)

    @property
    def elementary_links(self):
        """Identifiers of all elementary-link entangled states that have gone into this entangled state.

        Note that many of these elementary-link entangled states may no longer exist as such,
        as the entangled qubits that made them up may have been measured out or discarded.

        Returns
        -------
        set of :class:`entanglement_tracker_service.LinkIdentifier`.
            All the link identifiers of the elementary-link entangled states.

        """
        return self._link_identifiers

    @property
    def entangled_nodes(self):
        """Nodes that share this entangled state.

        Returns
        -------
        set of int
            :prop:`netsquid.nodes.node.Node.ID` of all nodes that are expected to hold a qubit that is part
            of this entangled state.

        """
        return {qubit_identifier.node_id
                for qubit_identifier in self._entangled_qubits}

    @property
    def has_alive_qubits(self):
        """Whether any of the qubits in this entangled state are still alive, i.e. not measured out or discarded.

        Qubits are considered to be not alive if a time of death has been set for them.

        Returns
        -------
        bool
            True if there are still alive qubits, False otherwise.

        """
        for qubit_data in self._qubit_data.values():
            if qubit_data.time_of_death is None:
                return True
        return False

    def get_link_bell_index(self, link_identifier):
        """Get Bell index corresponding to a specific elementary-link entangled state.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state to get the Bell index of.

        Returns
        -------
        :class:`netsquid.qubits.ketstates.BellIndex` or None
            Bell index indicating what Bell state the elementary-link entangled state is thought to be.
            If None, the Bell index is not tracked.

        """
        return self._get_link_data(link_identifier).bell_index

    def set_link_bell_index(self, link_identifier, bell_index):
        """Set Bell index corresponding to a specific elementary-link entangled state.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state for which the Bell index is set.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
            Bell index indicating what Bell state the elementary-link entangled state is thought to be.
            If None, the Bell index is not tracked.

        """
        self._get_link_data(link_identifier).bell_index = bell_index

    def get_time_of_birth(self, node_id, link_identifier):
        """Get time at which an elementary-link entangled qubit was created.

        Parameters
        ----------
        node_id : int
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the qubit was created.
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`.
            All the link identifiers of the elementary-link entangled states.
            Identifier of the elementary-link entangled state the qubit is part of.

        Returns
        -------
        float or None
            Time at which the qubit was created.
            None if no time of birth is known.

        """
        return self._get_qubit_data(node_id=node_id, link_identifier=link_identifier).time_of_birth

    def set_time_of_birth(self, node_id, link_identifier, time_of_birth):
        """Set time at which an elementary-link entangled qubit was created.

        Parameters
        ----------
        node_id : int
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the qubit was created.
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`.
            All the link identifiers of the elementary-link entangled states.
            Identifier of the elementary-link entangled state the qubit is part of.
        time_of_birth : float
            Time at which an elementary-link entangled qubit was created.

        """
        qubit_data = self._get_qubit_data(node_id=node_id, link_identifier=link_identifier)
        qubit_data.time_of_birth = float(time_of_birth)

    def get_time_of_death(self, node_id, link_identifier):
        """Get time at which an elementary-link entangled qubit ceased to exist.

        The death of a qubit can be registered using either
        :meth:`entanglement_tracker_service.EntanglementInfo.unentangle_qubit` or
        :meth:`entanglement_tracker_service.EntanglementInfo.destroy_qubit`.

        Parameters
        ----------
        node_id : int
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the qubit was originally created.
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state the qubit was a part of.

        Returns
        -------
        float or None
            Time at which the qubit was destroyed.
            If None, the qubit still exists as far as this `EntanglementInfo` is concerned.

        """
        return self._get_qubit_data(node_id=node_id, link_identifier=link_identifier).time_of_death

    def set_time_of_death(self, node_id, link_identifier, time_of_death):
        """Set time at which an elementary-link entangled qubit ceased to exist.

        This does not "unentangle" the qubit; the qubit is still considered part of the entangled state after setting
        a time of death, even though it is considered to no longer exist.
        Considering a destroyed qubit part of the entangled state can e.g. be used to represent measuring one half of a
        Bell state in the Z basis.
        Such an operation in principle destroys the entire entangled state, but it can still be useful to track the
        entangled state that would exist if the measurement was not performed to make statements about expected
        measurement correlations.
        Updating the time of death can be e.g. be useful when making statements about how much decoherence
        an entangled state has undergone.

        Parameters
        ----------
        node_id : int
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the qubit was originally created.
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state the qubit was a part of.
        time_of_death : float
            Time at which the qubit was destroyed.

        """
        qubit_data = self._get_qubit_data(node_id, link_identifier)
        qubit_data.time_of_death = time_of_death

    def merge(self, other_entanglement_info):
        """Merge this `EntanglementInfo`s with another one to create an `EntanglementInfo` representing a larger state.

        The `EntanglementInfo` that is created by merging two smaller ones represents an entangled state
        that contains all entangled qubits that made up the entangled states represented by the smaller
        `EntanglementInfo`s.
        These qubits are all considered to share one big entangled state together now.
        Thus, this method is to be used when entangled states are combined into a bigger entangled state through
        e.g. a Bell-state measurement.
        This method should not be used to track multiple independent entangled states using a single `EntanglementInfo`.

        The `EntanglementInfo`s that are merged are themselves unaffected by the operation.
        The only thing that happens is that an new, additional, larger `EntanglementInfo` is created.

        Parameters
        ----------
        other_entanglement_info : :class:`entanglement_tracker_service.EntanglementInfo`
            `EntanglementInfo` to be merged with.

        Returns
        -------
        :class:`entanglement_tracker_service.EntanglementInfo`
            `EntanglementInfo` representing an entangled state containing all entangled qubits of the `EntanglementInfo`
            from which this method is called and `other_entanglement_info`.

        """
        new_entanglement_info = deepcopy(self)
        new_entanglement_info._link_identifiers.update(other_entanglement_info._link_identifiers)
        new_entanglement_info._qubit_data.update(other_entanglement_info._qubit_data)
        new_entanglement_info._link_data.update(other_entanglement_info._link_data)
        return new_entanglement_info

    def unentangle_qubit(self, node_id, link_identifier, time_of_unentangling):
        """Register that a qubit is no longer entangled with the other qubits in the entangled state.

        After a qubit is "unentangled", the `EntanglementInfo` still considers all qubits that were entangled before
        to be entangled with one another.
        Thus, this method can be used to represent e.g. measuring a qubit that is part of a GHZ state in the
        Hadamard basis, but not in the computational basis.
        When a qubit is "unentangled", it is also considered "destroyed".
        The time of the unentangling is used as the time of death of the qubit.

        Parameters
        ----------
        node_id : int
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the qubit was originally created.
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state the qubit was a part of.
        time_of_unentangling : float
            Time at which the qubit was unentangled from the other states.
            This time is used as time of death of the qubit.

        """
        qubit_data = self._get_qubit_data(node_id, link_identifier)
        qubit_data.entangled = False
        self.set_time_of_death(node_id=node_id, link_identifier=link_identifier, time_of_death=time_of_unentangling)

    @property
    def _entangled_qubits(self):
        """Qubits that share the entangled state.

        This is the subset of all qubits of all elementary-link entangled states that went into creating
        this entangled states.
        Qubits are excluded from the set of entangled qubits if and only if they have been "unentangled" using
        :meth:`entanglement_tracker_service.EntanglementInfo.unentangle_qubit`.

        Returns
        -------
        list of _QubitIdentifier
            Identifiers of all qubits that have not been "unentangled".

        """
        return [qubit_identifier
                for qubit_identifier in self._qubit_identifiers
                if self._qubit_data[qubit_identifier].entangled]

    @property
    def _qubit_identifiers(self):
        """Identifiers of all elementary-link entangled qubits that went into creating this entangled state.

        Returns
        -------
        list of _QubitIdentifier
            Identifiers of all elementary-link entangled qubits.

        """
        return [self._QubitIdentifier(link_identifier, node_id)
                for link_identifier in self.elementary_links
                for node_id in link_identifier.node_ids]

    def _get_link_data(self, link_identifier):
        """Retrieve data about a specific elementary-link entangled state that went into this entangled state.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state of which data is requested.

        Returns
        -------
        _LinkData
            Data about the elementary-link entangled state.

        """
        return self._link_data[link_identifier]

    def _get_qubit_data(self, node_id, link_identifier):
        """Retrieve data about a specific elementary-link entangled qubit that went into this entangled state.

        Parameters
        ----------
        node_id : int
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the qubit was originally created.
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state the qubit was a part of.

        Returns
        -------
        _QubitData
            Data about the qubit.
        """
        qubit_identifier = self._QubitIdentifier(node_id=node_id, link_identifier=link_identifier)
        return self._qubit_data[qubit_identifier]


class BellStateEntanglementInfo(EntanglementInfo):
    """Holder for information about a Bell state shared across two different nodes.

    This is a subclass of `entanglement_tracker_service.EntanglementInfo` made specifically
    for tracking Bell states in a quantum network.
    Apart from tracking everything that `EntanglementInfo` does, it additionally tracks
    - Bell indices associated with entanglement swapping.
    - Bell index of the entangled state represented by this `BellStateEntanglementInfo`.

    To handle entanglement swapping, use the method
    :meth:`entanglement_tracker_service.BellStateEntanglementInfo.merge_by_swap`.
    It is discouraged to use the regular merge method,
    :meth:`entanglement_tracker_service.BellStateEntanglementInfo.merge`,
    since this does not guarantee that the resulting `EntanglementInfo` still represents a Bell state.

    Parameters
    ----------
    link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the elementary-link entangled state for which entanglement information should be stored.
    bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
        Index indicating what Bell state the elementary-link entangled state is.
        If None, the bell index is not tracked.
    times_of_birth : dict or float or None
        Times at which the qubits of the elementary-link entangled state indicated by `link_identifier`
        came into existence.
        Dictionary should have the :prop:`netsquid.nodes.node.Node.ID` (int) as keys
        (to refer to specific qubits in the elementary-link entangled state),
        and float or None as values (the time at which the corresponding qubit came into existence).
        If float or None is passed instead of a dictionary, this is used as the time of birth for all qubits
        in the elementary-link entangled state.
        When None is used as a time of birth, the time of birth is not tracked.

    """
    def __init__(self, link_identifier, bell_index=None, times_of_birth=None):
        super().__init__(link_identifier=link_identifier, bell_index=bell_index, times_of_birth=times_of_birth)
        self._swap_bell_indices = {}

    @property
    def bell_index(self):
        """Bell index indicating what Bell state the tracked entangled state is expected to be.

        The Bell index is determined by combining all Bell indices corresponding to elementary-link entangled states
        and entanglement swaps, and then counting the number of X and Z corrections (using |Bij> = Z^i X^j |B00>).
        Since all these corrections can be commuted through the state at every point in time, they can all be
        considered to be applied to one of the currently entangled qubits.

        Returns
        -------
        :class:`netsquid.qubits.ketstates.BellIndex` or None
            Bell index corresponding to the Bell state that this `BellStateEntanglementInfo` represents.
            None is returned in case not all elementary-link and swap Bell indices are tracked.
            In that case, no final bell index can be determined.

        """
        bell_indices = [self._get_link_data(link_identifier).bell_index
                        for link_identifier in self.elementary_links]
        bell_indices += list(self._swap_bell_indices.values())
        if None in bell_indices:
            return None
        num_b01 = bell_indices.count(BellIndex.B01)
        num_b10 = bell_indices.count(BellIndex.B10)
        num_b11 = bell_indices.count(BellIndex.B11)
        num_x = num_b01 + num_b11  # total number of X corrections
        num_z = num_b10 + num_b11  # total number of Z corrections
        x_correction = num_x % 2  # X ** 2 = I, so we only care about mod 2
        z_correction = num_z % 2  # Z ** 2 = I, so we only care about mod 2
        if x_correction:
            if z_correction:
                return BellIndex.B11  # both an X and Z correction
            else:
                return BellIndex.B01  # only X correction
        else:
            if z_correction:
                return BellIndex.B10  # only Z correction
            else:
                return BellIndex.B00  # no correction

    def merge(self, other_entanglement_info):
        """Merge this `EntanglementInfo`s with another one to create an `EntanglementInfo` representing a larger state.

        This method does the same as :meth:`entanglement_tracker_service.EntanglementInfo.merge`,
        but additionally also merges information about swap Bell indices.
        It is recommended to not use this method, but instead use
        :meth:`entanglement_tracker_service.BellStateEntanglementInfo.merge_by_swap` when possible.

        The `EntanglementInfo` that is created by merging two smaller ones represents an entangled state
        that contains all entangled qubits that made up the entangled states represented by the smaller
        `EntanglementInfo`s.
        These qubits are all considered to share one big entangled state together now.
        Thus, this method is to be used when entangled states are combined into a bigger entangled state through
        e.g. a Bell-state measurement.
        This method should not be used to track multiple independent entangled states using a single `EntanglementInfo`.

        The `EntanglementInfo`s that are merged are themselves unaffected by the operation.
        The only thing that happens is that an new, additional, larger `EntanglementInfo` is created.

        Parameters
        ----------
        other_entanglement_info : :class:`entanglement_tracker_service.BellStateEntanglementInfo`
            `BellStateEntanglementInfo` to be merged with.

        Returns
        -------
        :class:`entanglement_tracker_service.BellStateEntanglementInfo`
            `EntanglementInfo` representing an entangled state containing all entangled qubits of the `EntanglementInfo`
            from which this method is called and `other_entanglement_info`.

        """
        new_entanglement_info = super().merge(other_entanglement_info)
        new_entanglement_info._swap_bell_indices.update(other_entanglement_info._swap_bell_indices)
        return new_entanglement_info

    def merge_by_swap(self, other_entanglement_info, link_identifier_1, link_identifier_2, time_of_swap,
                      bell_index=None):
        """Merge two `BellStateEntanglementInfo`s to reperesent an entanglement swap (i.e. Bell-state measurement).

        This method can be used to represent an entanglement swap between two Bell states,
        thereby creating a new Bell state shared by qubits that were previously unentangled.

        What this method does is:
        - Merge the two `BellStateEntanglementInfo`s into one.
        - "Unentangle" the qubits that were measured out as part of the entanglement swap.
        - Register the Bell index indicating the outcome of the Bell-state measurement.

        This method determines which qubits were involved in the entanglement swap by looking at what node the two
        link-identifier parameters overlap.

        Parameters
        ----------
        other_entanglement_info : :class:`entanglement_tracker_service.BellStateEntanglementInfo`
            `BellStateEntanglementInfo` that is swapped with this `BellStateEntanglementInfo`.
        link_identifier_1 : :class:`entanglement_tracker_service.EntanglementInfo`
            Elementary-link entangled state as part of which the first qubit that partakes in the swap was created.
        link_identifier_2 : :class:`entanglement_tracker_service.EntanglementInfo`
            Elementary-link entangled state as part of which the second qubit that partakes in the swap was created.
            `link_identifier_2.node_ids` must have exactly one node overlap with `link_identifier_1`,
            it is assumed that the swap took place at the overlapping node.
        time_of_swap : float
            Time at which the entanglement swap took place.
            This is used as a time of death for the qubits that were measured out during the swap.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
            Bell-index indicating the outcome of the Bell-state measurement.
            If None, Bell index is not tracked.

        Returns
        -------
        :class:`entanglement_tracker_service.BellStateEntanglementInfo`
            `EntanglementInfo` representing the Bell state that is obtained by performing an entanglement swap
            between two two Bell states represented by the `EntanglementInfo` from which this method is called,
            and `other_entanglement_info`.

        """
        overlapping_nodes = [node_id for node_id in link_identifier_1.node_ids if node_id in link_identifier_2.node_ids]
        if len(overlapping_nodes) != 1:
            raise ValueError("There is not one overlapping node between the link identifiers.")
        [overlapping_node] = overlapping_nodes
        new_entanglement_info = self.merge(other_entanglement_info)
        new_entanglement_info._swap_bell_indices[frozenset({link_identifier_1, link_identifier_2})] = bell_index
        new_entanglement_info.unentangle_qubit(node_id=overlapping_node,
                                               link_identifier=link_identifier_1,
                                               time_of_unentangling=time_of_swap)
        new_entanglement_info.unentangle_qubit(node_id=overlapping_node,
                                               link_identifier=link_identifier_2,
                                               time_of_unentangling=time_of_swap)

        return new_entanglement_info

    def swap_bell_index(self, link_identifier_1, link_identifier_2):
        """Bell index of the outcome of the entanglement swap between two elementary-link entangled states.

        Parameters
        ----------
        link_identifier_1 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of first elementary-link state that partook in the entanglement swap.
        link_identifier_2 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of second elementary-link state that partook in the entanglement swap.

        Returns
        -------
        :class:`netsquid.qubits.ketstates.BellIndex` or None
            Bell index of the outcome of the entanglement swap.
            None if no swap has been registered between the two elementary-link states,
            or if Bell index is not tracked.

        """
        return self._swap_bell_indices.get(frozenset({link_identifier_1, link_identifier_2}), None)


class EntanglementTrackerService(ServiceProtocol, metaclass=ABCMeta):
    """Service framework for entanglement trackers in quantum networks.

    The purpose of the entanglement tracker is twofold:
    1. Tracking entangled qubits at the node that the entanglement tracker runs on
    (this includes tracking which entangled link is stored at which memory position).
    2. Tracking global entangled states.

    This service provides methods to register information about entanglement and methods to access the knowledge
    of both local and global entanglement.
    This knowledge is constructed by the entanglement tracker by combining all pieces of information that have
    been registered.
    This knowledge is stored in a (subclass of) :class:entanglement_tracker_service.EntanglementInfo`.
    Which subclass of `EntanglementInfo` is used is specific to the implementation of `EntanglementTrackerService`,
    and should be stored as the class attribute `EntanglementTrackerService.EntanglementInfo`.

    Information can be registered using either the different methods that have been defined for this service,
    such as `register_new_local_link`, or using requests.

    Whenever the status of local entangled qubits is changed (i.e. new qubits are created or discarded/measured out),
    this service sends a response (`ResNewLocalLink`, `ResLocalDiscard` or `ResLocalMeasure`) so that protocols
    using entangled qubits can easily remain up to date.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on
    name : str, optional
        The name of this protocol

    """
    EntanglementInfo = EntanglementInfo

    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self.register_request(req_type=ReqNewLinkUpdate, handler=self.handle_req_new_link_update)
        self.register_request(req_type=ReqDiscardUpdate, handler=self.handle_req_discard_update)
        self.register_request(req_type=ReqMeasureUpdate, handler=self.handle_req_measurement_update)
        self.register_request(req_type=ReqSwapUpdate, handler=self.handle_req_swap_update)
        self.register_response(ResNewLocalLink)
        self.register_response(ResLocalMeasure)
        self.register_response(ResLocalDiscard)
        self._largest_used_pair_identifier = {}  # remote node ids as keys, integers as values

    @property
    @abstractmethod
    def num_available_links(self):
        """Number of usable elementary-link entangled states currently available at this node.

        Returns
        -------
        list of :class:`entanglement_tracker_service.LinkIdentifier`
            All known link identifiers at this node with the status
            :atr:`entanglement_tracker_service.LinkStatus.AVAILABLE`.

        """
        return 0

    @abstractmethod
    def register_new_link(self, link_identifier, bell_index=None, mem_pos=None, times_of_birth=None):
        """Register the creation of a new entangled state on an elementary link.

        The entangled state can either be local to the node that this service runs on
        (i.e. one of the qubits is located at the node) or not.
        The memory position `mem_pos` is only relevant if the link is local.

        If a link is registered for the second time, any information (i.e. `bell_index`, `mem_pos` or `times_of_birth`)
        that is provided that was previously not tracked (i.e. registered as `None`) will be tracked from now on.
        The new information must be consistent with all previously tracked information.

        Parameters
        ----------
        link_identifier: :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the newly-generated elementary link.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
            Index of the Bell state that was generated.
            if None, the Bell index is not tracked.
        mem_pos : int or None
            Memory position at which the part of the new entangled state located at this node is stored.
            If None, the memory position of this link will not be tracked.
            If the node this service runs on does not share in the entanglement, it should be None.
        times_of_birth : dict or float or None
            Times at which the qubits of the elementary-link entangled state indicated by `link_identifier`
            came into existence.
            Dictionary should have the :prop:`netsquid.nodes.node.Node.ID` (int) as keys
            (to refer to specific qubits in the elementary-link entangled state),
            and float or None as values (the time at which the corresponding qubit came into existence).
            If float or None is passed instead of a dictionary, this is used as the time of birth for all qubits
            in the elementary-link entangled state.
            When None is used as a time of birth, the time of birth is not tracked.

        Raises
        ------
        RunTimeError
            If a `link_identifier` is registered that has already been registered before, and the registered
            information (`bell_index`, `mem_pos` or `times_of_birth`) is inconsistent with previously tracked
            information about the link.

        """
        if not isinstance(link_identifier, LinkIdentifier):
            raise TypeError(f"{link_identifier} is not a LinkIdentifier.")
        if bell_index is not None:
            BellIndex(bell_index)
        if not isinstance(mem_pos, int) and mem_pos is not None:
            raise TypeError(f"{mem_pos} is not an integer or None.")
        [remote_node_id] = [node_id for node_id in link_identifier.node_ids if node_id != self.node.ID]
        if remote_node_id not in self._largest_used_pair_identifier \
                or link_identifier.pair_identifier > self._largest_used_pair_identifier[remote_node_id]:
            self._largest_used_pair_identifier[remote_node_id] = link_identifier.pair_identifier

    def register_new_local_link(self, remote_node_id, bell_index=None, mem_pos=None, time_of_birth=None):
        """Register the creation of a new entangled state on an elementary link at this node.

        This method automatically generates a unique :class:`entanglement_tracker_service.LinkIdentifier`
        and then calls :meth:`entanglement_tracker_service.EntanglementTrackerService`.

        Note that this method does not guarantee that this node and the remote node use the same
        :class:`entanglement_tracker_service.LinkIdentifier.pair_identifier`.
        This should be the case though if all links shared between the nodes are always registered using this method
        on both nodes.

        Parameters
        ----------
        remote_node_id : int
            :prop:`netsquid.nodes.node.Node.ID` of the node that this node has generated entanglement with.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
            Index of the Bell state that was generated.
            If None, the Bell index is not tracked.
        mem_pos : int or None
            Memory position at which the part of the new entangled state located at this node is stored.
            If None, the memory position of this link will not be tracked.
            If the node this service runs on does not share in the entanglement, it should be None.
        time_of_birth : float or None
            Time at which the link was generated (i.e. when the local entangled qubit came into existence).
            If None, the time at which this method is called is used instead.

        Notes
        -----
        This method generates a pair identifier by taking the largest pair identifier that has already been used
        for link between this node and the corresponding remote node, and adding one.

        """
        if remote_node_id not in self._largest_used_pair_identifier:
            self._largest_used_pair_identifier[remote_node_id] = 0
        else:
            self._largest_used_pair_identifier[remote_node_id] += 1
        link_identifier = LinkIdentifier(node_ids=frozenset({self.node.ID, remote_node_id}),
                                         pair_identifier=self._largest_used_pair_identifier[remote_node_id])
        time_of_birth = ns.sim_time() if time_of_birth is None else time_of_birth
        times_of_birth = {self.node.ID: time_of_birth, remote_node_id: None}
        self.register_new_link(link_identifier=link_identifier,
                               bell_index=bell_index,
                               mem_pos=mem_pos,
                               times_of_birth=times_of_birth)

    @abstractmethod
    def register_discard(self, link_identifier, node_id=None, time_of_discard=None):
        """Register the discard of an entangled qubit.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state as part of which the discarded qubit was created.
        node_id : int or None
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the discard has taken place.
            If None, the ID of the node this service runs on is used.
        time_of_discard : float or None
            Time at which the qubit was discarded.
            If None, the time at which this request is issued is used instead.

        Raises
        ------
        ValueError
            If `link_identifier.node_ids` does not contain `node_id`.

        """
        if not isinstance(link_identifier, LinkIdentifier):
            raise TypeError(f"{link_identifier} is not a LinkIdentifier.")
        if node_id not in link_identifier.node_ids:
            raise ValueError(f"node ID {node_id} not in link_identifier.node_ids")

    def register_local_discard_mem_pos(self, mem_pos, time_of_discard=None):
        """Register the discard of a qubit at a specific memory position.

        This function tries to match the memory position to a
        :class:`entanglement_tracker_service.LinkIdentifier`,
        using meth:`entanglement_tracker_service.EntanglementTrackerService.get_link`.
        If this is successful,
        :meth:`entanglement_tracker_service.EntanglementTrackerService.register_local_discard` is called.
        However, if no  :class:`entanglement_tracker_service.LinkIdentifier` is found,
        this method does nothing.

        Parameters
        ----------
        mem_pos : int
            Memory position of the qubit that was discarded.
        time_of_discard : float
            Time at which the qubit was discarded.
            If None, the time at which this request is issued is used instead.

        """
        link_identifier = self.get_link(mem_pos)
        if link_identifier is not None:
            self.register_discard(link_identifier=link_identifier,
                                  node_id=self.node.ID,
                                  time_of_discard=time_of_discard)

    @abstractmethod
    def register_measurement(self, link_identifier, node_id=None, time_of_measurement=None):
        """Register the measurement of an entangled qubit.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state as part of which the measured qubit was created.
        node_id : int or None
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the measurement has taken place.
            If None, the ID of the node this service runs on is used.
        time_of_measurement : float or None
            Time at which the qubit was measured.
            If None, the time at which this request is issued is used instead.

        Raises
        ------
        ValueError
            If `link_identifier.node_ids` does not contain `node_id`.

        """
        if not isinstance(link_identifier, LinkIdentifier):
            raise TypeError(f"{link_identifier} is not a LinkIdentifier.")
        if node_id not in link_identifier.node_ids:
            raise ValueError(f"node ID {node_id} not in link_identifier.node_ids")

    def register_local_measurement_mem_pos(self, mem_pos, time_of_measurement=None):
        """Register the measurement of a local qubit at a specific memory position.

        This function tries to match the memory position to a
        :class:`entanglement_tracker_service.LinkIdentifier`,
        using meth:`entanglement_tracker_service.EntanglementTrackerService.get_link`.
        If this is successful,
        :meth:`entanglement_tracker_service.EntanglementTrackerService.register_measurement` is called.
        However, if no  :class:`entanglement_tracker_service.LinkIdentifier` is found,
        this method does nothing.

        Parameters
        ----------
        mem_pos : int
            Memory position of the qubit that was measured.
        time_of_measurement : float
            Time at which the qubit was measured.
            If None, the time at which this request is issued is used instead.

        """
        link_identifier = self.get_link(mem_pos)
        if link_identifier is not None:
            self.register_measurement(link_identifier=link_identifier,
                                      node_id=self.node.ID,
                                      time_of_measurement=time_of_measurement)

    @abstractmethod
    def register_swap(self, link_identifier_1, link_identifier_2, bell_index=None, time_of_swap=None):
        """Register the performance of a bipartite entanglement swap (i.e. a Bell-state measurement).

        The entanglement swap can have taken place at either this or any other node.

        Parameters
        ----------
        link_identifier_1 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of one of the two elementary-link states that were swapped.
        link_identifier_2 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the other elementary-link state that was swapped.
            `link_identifier_2.node_ids` must have a single node ID overlap with `link_identifier_1.node_ids`.
            It is assumed the swap took place at the node with this ID.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
            Bell index of the outcome of the Bell-state measurement.
            If None, Bell index is not tracked.
        time_of_swap : float or None
            Time at which the swap was performed.
            If None, the time at which this request is issued is used instead.

        """
        for link_identifier in [link_identifier_1, link_identifier_2]:
            if not isinstance(link_identifier, LinkIdentifier):
                raise TypeError(f"{link_identifier} is not a LinkIdentifier.")
        if bell_index is not None:
            BellIndex(bell_index)

    def register_local_swap_mem_pos(self, mem_pos_1, mem_pos_2, bell_index=None, time_of_swap=None):
        """Register the performance of a local entanglement swap at this node using memory positions.

        Note
        ----
        This function tries to match the memory positions to
        :class:`entanglement_tracker_service.LinkIdentifier`s,
        using meth:`entanglement_tracker_service.EntanglementTrackerService.get_link`.
        If this is successful, :meth:`entanglement_tracker_service.EntanglementTrackerService.register_local_swap`
        is called.
        If no :class:`entanglement_tracker_service.LinkIdentifier` is found for any of the memory
        positions, this method does nothing. No error is raised, since it might be that the tracking functionality
        is simply not used.
        However, if no  :class:`entanglement_tracker_service.LinkIdentifier` is found for one of the memory
        positions, but it is for the other, a ValueError is raised because this suggests something went wrong.

        Parameters
        ----------
        mem_pos_1 : int
            Identifier of one of the memory positions that the swap was performed on.
        mem_pos_2 : int
            Identifier of the other elementary-link state that was swapped.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex`
            Bell index of the outcome of the Bell-state measurement.
            If None, Bell index is not tracked.
        time_of_swap : float or None
            Time at which the swap was performed.
            If None, the time at which this request is issued is used instead.

        Raises
        ------
        ValueError
            If an :class:`entanglement_tracker_service.LinkIdentifier` is found for one of the memory positions
            but not the other.

        """
        link1 = self.get_link(mem_pos_1)
        link2 = self.get_link(mem_pos_2)
        num_of_nones = len([link for link in [link1, link2] if link is None])
        if num_of_nones == 0:
            self.register_swap(link_identifier_1=link1,
                               link_identifier_2=link2,
                               bell_index=bell_index,
                               time_of_swap=time_of_swap)
        elif num_of_nones == 1:
            raise ValueError(f"Swapping on memory positions {mem_pos_1} and {mem_pos_2}. "
                             f"For one of these memory positions a elementary-link entangled state is known, "
                             f"for the other not.")

    @abstractmethod
    def register_move(self, old_mem_pos, new_mem_pos):
        """Register the move of a qubit from one memory position to another.

        If a elementary-link entangled-state qubit is known to be stored at `old_mem_pos`, this knowledge is changed
        such that it is now known to be stored at `new_mem_pos`.
        If no such qubit is known, this method does nothing.

        Parameters
        ----------
        old_mem_pos : int
            Memory position that held the qubit before the move.
        new_mem_pos : int
            Memory position that holds the qubit after the move.

        """
        pass

    @abstractmethod
    def get_status(self, link_identifier):
        """Get the status of local part of an elementary-link entangled state.

        Note that this method is only defined for elementary-link entangled states of which a qubit
        is stored at the node that this service runs on.
        It returns the status of this qubit, which can either be "available", "measured" or "discarded".

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state that the status is requested of.

        Returns
        -------
        :class:`entanglement_tracker_service.LinkStatus` or None
            Status of the elementary-link entangled state.
            If no status is known, None is returned.

        """
        return None

    @abstractmethod
    def get_mem_pos(self, link_identifier):
        """Get the memory position that holds the local part of a specific elementary-link entangled state.

        Note that this method is only defined for elementary-link entangled states of which a qubit
        is stored at the node that this service runs on.
        It returns the memory position of this qubit.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state that the memory position is requested of.

        Returns
        -------
        int or None
            Memory position that holds the qubit that was created as part of the elementary-link entangled state.
            If no memory position is known, or the status of the link is not
            :atr:`entanglement_tracker_service.LinkStatus.AVAILABLE`, None is returned.

        """
        return None

    @abstractmethod
    def get_link(self, mem_pos):
        """Get the identifier of the elementary-link entangled state of which part is stored at this memory position.

        Parameters
        ----------
        mem_pos : int
            Memory position at memory of this node at which to look for a qubit that was created as part of an
            elementary-link entangled state.

        Returns
        -------
        :class:`entanglement_tracker_service.LinkIdentifier` or None
            Identifier of the elementary-link entangled state of which part is stored in the memory position.
            If no such identifier is known, None is returned.

        """
        return None

    @abstractmethod
    def get_links_by_status(self, link_status):
        """Get all identifiers of local elementary-link entangled states with a specific status.

        Note that since the status of a link is only locally defined (it is the status of a local qubit that was
        created as part of an elementary-link entangled state), this only returns link identifiers corresponding
        to elementary-link entangled states local to the node this service runs on.

        Parameters
        ----------
        link_status : :class:`entanglement_tracker_service.LinkStatus`
            Status for which to find link identifiers.

        Returns
        -------
        list of :class:`entanglement_tracker_service.LinkIdentifier`
            All known link identifiers for which the entangled state has this status.

        """
        return []

    @abstractmethod
    def get_local_link_lifetime(self, link_identifier):
        """Get the lifetime of a qubit that was created locally as part of an elementary-link entangled state.

        Note that this is about local lifetime: it is the amount of time one specific qubit has spent in local memory.
        This method also works for expired links, i.e. those that do not have the status
        :atr:`entanglement_tracker_service.LinkStatus.AVAILABLE`.
        When a link expires, the lifetime is frozen.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state of which the local qubit's lifetime is requested.

        Returns
        -------
        float
            The local lifetime of the qubit.

        """
        return 0

    @abstractmethod
    def get_bell_index(self, link_identifier):
        """Get the Bell index corresponding to an elementary-link entangled state of which one qubit is local.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state of which the local qubit's lifetime is requested.

        Returns
        -------
        :class:`netsquid.qubits.ketstates.BellIndex` or None
            Index indicating what Bell state the elementary-link entangled state is thought to be.
            If None, the Bell index is not tracked.

        """
        return None

    @abstractmethod
    def get_entanglement_info_of_link(self, link_identifier):
        """Get entanglement information about a qubit that was created as part of an elementary-link entangled state.

        The entanglement information reflects what this entanglement tracker believes to be true about global
        entanglement, rather than what is actually true.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state about which the local qubit's entanglement info is
            requested.

        Returns
        -------
        :class:`entanglement_tracker_service.EntanglementInfo`
            Holder of information about the entangled state that the qubit is part of.

        """
        return EntanglementInfo(link_identifier, BellIndex.B00, times_of_birth=None)

    @abstractmethod
    def get_entanglement_info_between_nodes(self, node_ids, other_nodes_allowed=False, must_be_intact=True):
        """Get all entanglement information about entanglement between specific nodes.

        Examples
        --------
        1. When Alice wants to know if she shared bipartite entanglement with Bob, e.g. to be used in QKD, she can call
         `get_entanglement_info_between_nodes(node_ids={node_alice.ID, node_bob.ID}, other_nodes_allowed=False)`.

        2. When Charlie wants to know what entanglement she shares with any other nodes in the network, she can call
         `get_entanglement_info_between_nodes(node_ids={node_charlie.ID}, other_nodes_allowed=True)`.

        Parameters
        ----------
        node_ids : set of int
            :prop:`netsquid.nodes.node.Node.ID` of nodes for which entanglement information is requested.
        other_nodes_allowed : bool
            True if the returned entanglement information can be about entangled states which are not just shared
            by the nodes in `node_ids`, but also by additional nodes.
            False if the returned entanglement information must describe entanglement between the nodes in `node_ids`
            and no other nodes.
        must_be_intact : bool
            True if only entanglement information for which `EntanglementInfo.intact == True` should be returned.
            An `EntanglementInfo` that is not "intact" represents an entangled state that is thought to have been
            destroyed somehow, rendering the qubit no longer entangled.

        Returns
        -------
        list of :class:`entanglement_tracker_service.EntanglementInfo`
            All available entanglement information about entangled states that include the nodes in `node_ids`.

        """
        return []

    @abstractmethod
    def await_entanglement(self, node_ids, link_identifiers=set(), other_nodes_allowed=False):
        """Wait for an entangled state between specific nodes based on specific elementary-link entangled states.

        Wait until the entanglement tracker believes there is an entangled state shared between the required
        nodes, that has been built using the required elementary-link entangled states.
        That is, until there is an `EntanglementInfo` available that includes `node_ids` in
        `EntanglementInfo.entangled_nodes` and `link_identifiers` in `EntanglementInfo.elementary_links`.

        In case the entanglement tracker believes that one of the required elementary-link entangled states
        is no longer part of an intact entangled state, obtaining the target entangled state is deemed impossible
        and the wait is stopped.
        That is, this method waits until either an `EntanglementInfo` is available that fulfills the conditions
        described above, or when there is an `EntanglementInfo` that includes one of the `link_identifiers`
        in `EntanglementInfo.elementary_links` for which `EntanglementInfo.intact == False`.

        To have a protocol await entanglement, put `yield from tracker.await_entanglement()` in the
        `run()` method of the protocol, where `tracker` is this entanglement tracker.

        Examples
        --------
        1. Alice and Bob are continuously generating entanglement through e.g. a repeater chain.
         Alice can consume her entanglement as soon as it becomes available by calling `await_entanglement`
         with `node_ids = {Alice.ID, Bob.ID}`.
         After the wait, she locates the entangled qubit using e.g. `get_entanglement_info_between_nodes`
         and consumes it.
         She can then again start waiting for the next entanglement.

        2. Alice created entanglement with her neighbour, which is part of a repeater chain.
         She now hopes that the repeater chain will entangle her qubit with Bob through entanglement swapping.
         However, one of the repeaters in the chain might also destroy the entangled state when a cutoff time is met.
         To this end, Alice uses `await_entanglement` with `node_ids = {Alice.ID, Bob.ID}` and
         `link_identifiers = {link_with_neighbour_identifier}`.
         After the wait is over, she checks whether her qubit is entangled with Bob, or whether the entanglement was
         destroyed (e.g. by checking the value of `get_entanglement_of_link(link_with_neighbour_identifier).intact`).
         She can then either use her entangled qubit for some application, or try again.

        Parameters
        ----------
        node_ids : set of int
            :prop:`netsquid.nodes.node.Node.ID` of the nodes that should share the entangled state that is waited for.
        link_identifiers : set of LinkIdentifier
            Identifiers of elementary-link states that should have gone into the entangled state that is waited for.
        other_nodes_allowed : bool
            True if the entangled state that is waited for can not only be shared between the nodes in `node_ids`,
            but also by additional nodes.
            False if the entangled state must be just between the nodes in `node_ids` and no other nodes.

        """
        yield

    def is_local(self, link_identifier):
        """Determine if an elementary-link entangled state is local to the node this service runs on.

        `link_identifier` is considered local iff `link_identifier.node_ids` contains the local node's
        :prop:`netsquid.nodes.node.Node.ID`.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of elementary-link entangled state to check for locality.

        Returns
        -------
        bool
            True if the link_identifier is local, False otherwise.

        """
        return self.node.ID in link_identifier.node_ids

    def handle_req_new_link_update(self, req):
        """Handle request to update information about a newly generated elementary-link entangled state.

        Calls the method
        :meth:`entanglement_tracker_service.EntanglementTrackerService.register_new_link`
        using the parameters handed by the request.
        Note that these requests cannot convey memory positions. They are primarily meant for communicating
        updates across a network. To register a link locally, it is better ot call the method
        :meth:`entanglement_tracker_service.EntanglementTrackerService.register_new_link` directly.

        Parameters
        ----------
        req : :class:`entanglement_tracker_service.ReqNewLinkUpdate`
            Request containing the information about the new elementary-link entangled state.

        """
        assert isinstance(req, ReqNewLinkUpdate)
        self.register_new_link(link_identifier=req.link_identifier,
                               bell_index=req.bell_index,
                               times_of_birth=req.times_of_birth,
                               mem_pos=None)

    def handle_req_discard_update(self, req):
        """Handle request to update knowledge with information about the discard of an entangled qubit.

        Calls the method
        :meth:`entanglement_tracker_service.EntanglementTrackerService.register_discard`
        using the parameters handed by the request.

        Parameters
        ----------
        req : :class:`entanglement_tracker_service.ReqDiscardUpdate`
            Request containing the information about the discard.

        """
        assert isinstance(req, ReqDiscardUpdate)
        self.register_discard(link_identifier=req.link_identifier,
                              node_id=req.node_id,
                              time_of_discard=req.time_of_discard)

    def handle_req_measurement_update(self, req):
        """Handle request to update knowledge with information about the measurement of an entangled qubit.

        Calls the method
        :meth:`entanglement_tracker_service.EntanglementTrackerService.register_measurement`
        using the parameters handed by the request.

        Parameters
        ----------
        req : :class:`entanglement_tracker_service.ReqDiscardUpdate`
            Request containing the information about the discard.

        """
        assert isinstance(req, ReqMeasureUpdate)
        self.register_measurement(link_identifier=req.link_identifier,
                                  node_id=req.node_id,
                                  time_of_measurement=req.time_of_measurement)

    def handle_req_swap_update(self, req):
        """Handle request to update information about a an entanglement swap.

        Calls the method
        :meth:`entanglement_tracker_service.EntanglementTrackerService.register_swap`
        using the parameters handed by the request.

        Parameters
        ----------
        req : :class:`entanglement_tracker_service.ReqSwapUpdate`
            Request containing the information about the new elementary-link entangled state.

        """
        assert isinstance(req, ReqSwapUpdate)
        self.register_swap(link_identifier_1=req.link_identifier_1,
                           link_identifier_2=req.link_identifier_2,
                           bell_index=req.bell_index,
                           time_of_swap=req.time_of_swap)
