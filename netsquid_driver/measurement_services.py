from abc import ABCMeta, abstractmethod
from dataclasses import dataclass

from netsquid.protocols.serviceprotocol import ServiceProtocol
from netsquid.qubits.ketstates import BellIndex


@dataclass
class ReqMeasure:
    """Request to perform a single-qubit measurement in any basis.

    Measurement bases can be manipulated by performing a general rotation on a qubit before measurement.
    Any rotation can be decomposes into first an X rotation, then an Y rotation, and then another X rotation.
    By specifying the three corresponding angles (these are Euler angles), the pre-measurement rotation is specified.

    """
    mem_pos: int = 0  # Memory position to perform measurement on.
    x_rotation_angle_1: float = 0  # Rotation angle in radians for first rotation, around x axis.
    y_rotation_angle: float = 0  # Rotation angle in radians for second rotation, around y axis.
    x_rotation_angle_2: float = 0  # Rotation angle in radians for third rotation, around x axis again.


@dataclass
class ResMeasure:
    """Response conveying the outcome of a single-qubit measurement."""
    outcome: bool or None = 0  # Outcome of measurement, None if measurement failed
    # TODO should we use an enum for measurement outcomes?


@dataclass
class ReqSwap:
    """Request to perform an entanglement swap (i.e. a Bell-state measurement)."""
    mem_pos_1: int = 0  # First memory position to use in entanglement swap.
    mem_pos_2: int = 1  # Second memory position to use in entanglement swap.


@dataclass
class ResSwap:
    """Response conveying the outcome of an entanglement swap (i.e. Bell-state measurement)."""
    outcome: BellIndex or None  # None if the entanglement failed, otherwise the Bell index corresponding to the
    # outcome of the Bell-state measurement.


class MeasureService(ServiceProtocol, metaclass=ABCMeta):
    """Service for performing a single-qubit measurement.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on
    name : str, optional
        The name of this protocol

    """
    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self.register_request(req_type=ReqMeasure, handler=self.measure)
        self.register_response(ResMeasure)

    @abstractmethod
    def measure(self, req):
        assert isinstance(req, ReqMeasure)


class SwapService(ServiceProtocol, metaclass=ABCMeta):
    """Service for entanglement swapping (i.e. performing a Bell-state measurement).

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on
    name : str, optional
        The name of this protocol

    """
    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self.register_request(req_type=ReqSwap, handler=self.swap)
        self.register_response(ResSwap)

    @abstractmethod
    def swap(self, req):
        """Perform entanglement swap

        Parameters
        ----------
        req : :class:`ReqSwap`
            Request that needs to be handled by this method.

        """
        assert isinstance(req, ReqSwap)
