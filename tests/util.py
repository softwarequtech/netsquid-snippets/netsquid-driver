from __future__ import annotations

from typing import List

from netsquid.nodes import Node
from netsquid_driver.classical_routing_service import ClassicalRoutingService
from netsquid_driver.driver import Driver
from netsquid_physlayer.classical_connection import ClassicalConnection


def create_linear_network_with_routing(node_names: List[str], connection_delay: float = 0):
    """
    Set up a linear network with classical message routing.

    :param node_names: A list of strings representing the names of the nodes in the network.
    :param connection_delay: Optional. The message delay in nanoseconds for each connection in the network.
     (default is 0)
    :return: A list of Node objects with a driver and a ClassicalRoutingService.
    """
    nodes = []
    for i, node_name in enumerate(node_names):
        node = Node(name=node_name)
        node.add_ports(["A", "B"])
        nodes.append(node)

        node.driver = Driver("driver")
        node.add_subcomponent(node.driver)

        local_routing_table = {}
        for j, remote_node_name in enumerate(node_names):
            if node_name is not remote_node_name:
                if j < i:
                    port = node.ports["A"]
                else:
                    port = node.ports["B"]

                local_routing_table[remote_node_name] = port

        routing_service = ClassicalRoutingService(node, local_routing_table)
        node.driver.add_service(ClassicalRoutingService, routing_service)

    for i in range(len(nodes) - 1):
        connection = ClassicalConnection(f"connection_{nodes[i].name}_{nodes[i + 1].name}", delay=connection_delay)
        nodes[i].connect_to(remote_node=nodes[i + 1],
                            connection=connection,
                            local_port_name="B", remote_port_name="A")

    return nodes
