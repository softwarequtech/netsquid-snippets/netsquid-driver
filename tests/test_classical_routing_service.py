import unittest
from dataclasses import dataclass
from typing import Any

import netsquid as ns
from netsquid.nodes import Node
from netsquid.protocols import ServiceProtocol
from netsquid_driver.classical_routing_service import ClassicalRoutingService, RemoteServiceRequest
from netsquid_driver.driver import Driver
from netsquid_physlayer.classical_connection import ClassicalConnection
from util import create_linear_network_with_routing


@dataclass
class MockRequest:
    origin_node: str
    payload: Any = None


class MockService(ServiceProtocol):
    def __init__(self, node):
        super().__init__(node)
        self.register_request(MockRequest, self.process_request)
        self.received_requests = []

    def process_request(self, req: MockRequest):
        self.received_requests.append(req)


class MockService2(ServiceProtocol):
    def __init__(self, node):
        super().__init__(node)
        self.register_request(MockRequest, self.process_request)
        self.received_requests = []

    def process_request(self, req: MockRequest):
        self.received_requests.append(req)


class ClassicalRoutingTests(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()

    @staticmethod
    def setup_broken_three_node_network():
        node_names = ["Node0", "Node1", "Node2"]
        nodes = []
        for i, node_name in enumerate(node_names):
            node = Node(name=node_name)
            node.add_ports(["A", "B"])
            nodes.append(node)

            node.driver = Driver("driver")
            node.driver.add_service(MockService, MockService(node))

        nodes[0].connect_to(remote_node=nodes[1],
                            connection=ClassicalConnection("connection"),
                            local_port_name="B", remote_port_name="A")
        nodes[1].connect_to(remote_node=nodes[2],
                            connection=ClassicalConnection("connection"),
                            local_port_name="B", remote_port_name="A")
        nodes[2].connect_to(remote_node=nodes[0],
                            connection=ClassicalConnection("connection"),
                            local_port_name="B", remote_port_name="A")

        # Node 0 incorrectly forwards messages for node2 to node1
        node_0_forwarding_table = {"Node1": nodes[0].ports["B"],
                                   "Node2": nodes[0].ports["B"]}
        # Node 1 incorrectly forwards messages for node 2 to node0
        node_1_forwarding_table = {"Node0": nodes[1].ports["A"],
                                   "Node2": nodes[1].ports["A"]}
        # Node 2 is set up correctly
        node_2_forwarding_table = {"Node0": nodes[2].ports["B"],
                                   "Node1": nodes[2].ports["A"]}

        routing_service = ClassicalRoutingService(nodes[0], node_0_forwarding_table)
        nodes[0].driver.add_service(ClassicalRoutingService, routing_service)
        routing_service = ClassicalRoutingService(nodes[1], node_1_forwarding_table)
        nodes[1].driver.add_service(ClassicalRoutingService, routing_service)
        routing_service = ClassicalRoutingService(nodes[2], node_2_forwarding_table)
        nodes[2].driver.add_service(ClassicalRoutingService, routing_service)

        for node in nodes:
            node.driver.start_all_services()

        return nodes

    def test_two_nodes_message(self):
        node_names = ["Node0", "Node1"]
        nodes = create_linear_network_with_routing(node_names)
        for node in nodes:
            node.driver.add_service(MockService, MockService(node))
            node.driver.start_all_services()

        req = MockRequest(nodes[0].name)
        remote_req = RemoteServiceRequest(req, MockService, origin=nodes[0].name, targets=[nodes[1].name])
        nodes[0].driver.services[ClassicalRoutingService].put(remote_req)
        ns.sim_run()

        node0_mock_service = nodes[0].driver.services[MockService]
        node1_mock_service = nodes[1].driver.services[MockService]
        assert len(node0_mock_service.received_requests) == 0
        assert len(node1_mock_service.received_requests) == 1

    def test_message_forwarding(self):
        node_names = ["Node0", "Node1", "Node2", "Node3", "Node4", "Node5"]
        nodes = create_linear_network_with_routing(node_names)
        for node in nodes:
            node.driver.add_service(MockService, MockService(node))
            node.driver.start_all_services()

        req = MockRequest(nodes[0].name)
        remote_req = RemoteServiceRequest(req, MockService, origin=nodes[0].name, targets=[nodes[5].name])
        nodes[0].driver.services[ClassicalRoutingService].put(remote_req)
        ns.sim_run()

        assert len(nodes[0].driver.services[MockService].received_requests) == 0
        assert len(nodes[1].driver.services[MockService].received_requests) == 0
        assert len(nodes[2].driver.services[MockService].received_requests) == 0
        assert len(nodes[3].driver.services[MockService].received_requests) == 0
        assert len(nodes[4].driver.services[MockService].received_requests) == 0
        assert len(nodes[5].driver.services[MockService].received_requests) == 1

    def test_two_nodes_message_payload_alteration(self):
        node_names = ["Node0", "Node1"]
        # original payload
        payload = [1, 2, 3]
        nodes = create_linear_network_with_routing(node_names)
        for node in nodes:
            node.driver.add_service(MockService, MockService(node))
            node.driver.start_all_services()

        req = MockRequest(nodes[0].name, payload)
        remote_req = RemoteServiceRequest(req, MockService, origin=nodes[0].name, targets=[nodes[1].name])
        nodes[0].driver.services[ClassicalRoutingService].put(remote_req)
        # Alter payload after sending a message
        payload.append(4)

        ns.sim_run()

        node0_mock_service = nodes[0].driver.services[MockService]
        node1_mock_service = nodes[1].driver.services[MockService]
        assert len(node0_mock_service.received_requests) == 0
        assert len(node1_mock_service.received_requests) == 1
        self.assertEqual(node1_mock_service.received_requests[0].payload, [1, 2, 3])
        self.assertNotEqual(id(node1_mock_service.received_requests[0].payload), id(payload))

    def test_message_broadcast(self):
        node_names = ["Node0", "Node1", "Node2", "Node3", "Node4", "Node5"]
        nodes = create_linear_network_with_routing(node_names)
        for node in nodes:
            node.driver.add_service(MockService, MockService(node))
            node.driver.start_all_services()

        req = MockRequest(nodes[0].name)
        remote_req = RemoteServiceRequest(req, MockService, origin=nodes[0].name, targets=[])
        nodes[0].driver.services[ClassicalRoutingService].broadcast(remote_req)
        ns.sim_run()

        assert len(nodes[0].driver.services[MockService].received_requests) == 0
        assert len(nodes[1].driver.services[MockService].received_requests) == 1
        assert len(nodes[2].driver.services[MockService].received_requests) == 1
        assert len(nodes[3].driver.services[MockService].received_requests) == 1
        assert len(nodes[4].driver.services[MockService].received_requests) == 1
        assert len(nodes[5].driver.services[MockService].received_requests) == 1

    def test_multiple_services(self):
        node_names = ["Node0", "Node1"]
        nodes = create_linear_network_with_routing(node_names)
        for node in nodes:
            node.driver.add_service(MockService, MockService(node))
            # Add extra mock service
            node.driver.add_service(MockService2, MockService2(node))
            node.driver.start_all_services()

        req = MockRequest(nodes[0].name)
        remote_req1 = RemoteServiceRequest(req, MockService2, origin=nodes[0].name, targets=[nodes[1].name])
        nodes[0].driver.services[ClassicalRoutingService].put(remote_req1)
        ns.sim_run()

        node0_mock1_service = nodes[0].driver.services[MockService]
        node1_mock1_service = nodes[1].driver.services[MockService]
        node0_mock2_service = nodes[0].driver.services[MockService2]
        node1_mock2_service = nodes[1].driver.services[MockService2]
        assert len(node0_mock1_service.received_requests) == 0
        assert len(node1_mock1_service.received_requests) == 0
        assert len(node0_mock2_service.received_requests) == 0
        assert len(node1_mock2_service.received_requests) == 1

    def test_target_self(self):
        node_names = ["Node0", "Node1"]
        nodes = create_linear_network_with_routing(node_names)
        for node in nodes:
            node.driver.add_service(MockService, MockService(node))
            node.driver.start_all_services()

        req = MockRequest(nodes[0].name)
        remote_req1 = RemoteServiceRequest(req, MockService, origin=nodes[0].name, targets=[nodes[0].name])
        nodes[0].driver.services[ClassicalRoutingService].put(remote_req1)
        ns.sim_run()

        node0_mock_service = nodes[0].driver.services[MockService]
        node1_mock_service = nodes[1].driver.services[MockService]
        assert len(node0_mock_service.received_requests) == 1
        assert len(node1_mock_service.received_requests) == 0

    def test_infinite_loop_user_error(self):
        nodes = self.setup_broken_three_node_network()
        req = MockRequest(nodes[0].name)
        remote_req = RemoteServiceRequest(req, MockService, origin=nodes[0].name, targets=[nodes[2].name])
        nodes[0].driver.services[ClassicalRoutingService].put(remote_req)

        with self.assertRaises(RuntimeError) as context:
            ns.sim_run()

            self.assertTrue("Message has exceeded maximum forwards" in context.exception)

        assert len(nodes[0].driver.services[MockService].received_requests) == 0
        assert len(nodes[1].driver.services[MockService].received_requests) == 0
        assert len(nodes[2].driver.services[MockService].received_requests) == 0
