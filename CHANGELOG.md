CHANGELOG
=========

2024-08-27 (2.0.0)
---------------------
Major update for integration with netsquid-netbuilder

Added
________

- classical_routing_service.py 
- classical_socket_service.py
- connectionless_socket_service.py
- operation_services.py  
  File copied from netsquid-qrepchain
- Memory_manager_implementations.py  
  File copied from netsquid-qrepchain
- Entanglement_tracker_service.py  
  File moved from netsquid-entanglementtracker

Changed
________

- Entanglement_agreement_service.py
   - Refactor: Created BaseEntanglementAgreement dataclass object that other requests and responses inherit from.
   - Removed EntanglementAgreementServiceWithMessages and its subclasses
- Initiative_based_agreement_service.py
   - Communication delay information to linked nodes now has to be given at initialization instead of service fetching it from connections.
   - Refactor: Created base dataclass, ReqBaseAgreementInteractionMessage, for inter-node communication of the service and the children: ReqAskAgreement, ReqConfirmAgreement, ReqRetractAskAgreement for the various interactions between instances of this service.
   - Refactor: _handle_message method has been split up into _handle_ask_agreement, _handle_confirm_agreement and _handle_retract_ask_agreement methods that are invoked at the matching request.
   - Refactor: Combine InitiativeAgreementService and RespondingAgreementService into one class. The instance will behave as either one depending on the mode parameter.
- Entanglement_service.py
   - Removed EntanglementServiceWithSingleNodePerPort
   - Removed HeraldedEntanglementBaseProtocol  
     A derivative of this protocol has been created in netsquid-magic.

Removed
________

- Message_handler_protocol.py  
  Functionality superseded by ClassicalRoutingService
- Swap_asap_services.py  
  Moved to netsquid-netbuilder


2022-11-30 (1.0.1)
------------------
- When entanglement is aborted in `HeraldedEntanglementBaseProtocol`, the error message in the response now has the value `EntanglementError.ABORTED`.

2022-06-21 (1.0.0)
------------------
-Exported code with driver and some service definitions from private repo on different gitlab instance.
