import netsquid as ns
from netsquid.nodes import Node
from netsquid.protocols import NodeProtocol
from netsquid_driver.EGP import EGPService
from qlink_interface import (
    ErrorCode,
    MeasurementBasis,
    RandomBasis,
    ReqCreateAndKeep,
    ReqMeasureDirectly,
    ReqReceive,
    ReqStopReceive,
    ResCreateAndKeep,
    ResError,
    ResMeasureDirectly,
    ResRemoteStatePrep,
)


class SimpleEGP(EGPService):

    def __init__(self, node):
        super().__init__(node=node, name="simple_EGP")
        self.performed_create_and_keep = False
        self.performed_measure_directly_fixed = False
        self.performed_create_and_measure_fixed_no_default_values = False
        self.performed_measure_directly_xz = False
        self.performed_measure_directly_xyz = False
        self.performed_measure_directly_chsh = False
        self.performed_remote_state_preparation = False
        self.performed_receive = False
        self.performed_stop_receive = False

    def start(self):
        super().start()

    def stop(self):
        super().stop()

    def create_and_keep(self, req):
        super().create_and_keep(req)
        self.performed_create_and_keep = True
        self.assert_req(req)
        create_id = self._get_create_id()
        self.send_response(ResCreateAndKeep(create_id=create_id))
        return create_id

    def measure_directly(self, req):
        super().measure_directly(req)
        assert req.random_basis_local == req.random_basis_remote
        create_id = self._get_create_id()
        if req.random_basis_local == RandomBasis.NONE:
            self.measure_directly_fixed(req, create_id)
        elif req.random_basis_local == RandomBasis.XZ:
            self.measure_directly_xz(req, create_id)
        elif req.random_basis_local == RandomBasis.XYZ:
            self.measure_directly_xyz(req, create_id)
        elif req.random_basis_local == RandomBasis.CHSH:
            self.measure_directly_chsh(req, create_id)
        else:
            raise RuntimeError("Could not identify measurement basis {}.".format(req.random_basis_local))
        return create_id

    def remote_state_preparation(self, req):
        super().remote_state_preparation(req)
        self.performed_remote_state_preparation = True
        self.assert_req(req)
        create_id = self._get_create_id()
        self.send_response(ResRemoteStatePrep(create_id=create_id))
        return create_id

    def measure_directly_fixed(self, req, create_id):
        if req.priority:  # this means that MockNetworkLayer.create_and_measure_fixed_no_default_values was called
            assert req.x_rotation_angle_local_1 == 10.
            self.performed_create_and_measure_fixed_no_default_values = True
        else:
            self.performed_measure_directly_fixed = True
            self.assert_req(req)
        self.send_response(ResMeasureDirectly(create_id=create_id))

    def measure_directly_xz(self, req, create_id):
        self.performed_measure_directly_xz = True
        self.assert_req(req)
        self.send_response(ResMeasureDirectly(create_id=create_id))

    def measure_directly_xyz(self, req, create_id):
        self.performed_measure_directly_xyz = True
        self.assert_req(req)
        self.send_response(ResMeasureDirectly(create_id=create_id))

    def measure_directly_chsh(self, req, create_id):
        self.performed_measure_directly_chsh = True
        self.assert_req(req)
        self.send_response(ResError(create_id=create_id, error_code=ErrorCode.UNSUPP))

    def receive(self, req):
        super().receive(req)
        self.performed_receive = True

    def stop_receive(self, req):
        super().stop_receive(req)
        self.performed_stop_receive = True

    @staticmethod
    def assert_req(req):
        assert req.remote_node_id == 0
        assert req.minimum_fidelity == 0
        assert req.time_unit == 0
        assert req.max_time == 0
        assert req.purpose_id == 0
        assert req.number == 1
        assert req.priority == 0
        assert not req.atomic
        assert not req.consecutive


class MockNetworkLayer(NodeProtocol):

    def __init__(self, node, egp):
        super().__init__(node=node, name="mock_network_layer")
        self.add_subprotocol(egp, "EGP")
        self.finished = False

    def start(self):
        super().start()

    def stop(self):
        super().stop()

    def run(self):

        egp = self.subprotocols["EGP"]
        egp.start()
        signal_label_keep = ResCreateAndKeep.__name__
        signal_label_meas = ResMeasureDirectly.__name__
        signal_label_error = ResError.__name__

        req = ReqCreateAndKeep()
        ack_create_id = egp.put(request=req)
        yield self.await_signal(sender=egp, signal_label=signal_label_keep)
        res_ok = egp.get_signal_result(signal_label_keep, self)

        assert ack_create_id == 0
        assert res_ok.create_id == 0
        assert res_ok.logical_qubit_id == 0

        req = ReqMeasureDirectly()
        egp.put(request=req)
        yield self.await_signal(sender=egp, signal_label=signal_label_meas)
        res_ok = egp.get_signal_result(signal_label_meas, self)

        assert res_ok.create_id == 1
        assert res_ok.measurement_outcome == 0
        assert res_ok.measurement_basis == MeasurementBasis.Z

        req = ReqMeasureDirectly(priority=True, x_rotation_angle_local_1=10.)
        egp.put(request=req)
        yield self.await_signal(sender=egp, signal_label=signal_label_meas)

        req = ReqMeasureDirectly(random_basis_local=RandomBasis.XZ,
                                 random_basis_remote=RandomBasis.XZ)
        egp.put(request=req)
        yield self.await_signal(sender=egp, signal_label=signal_label_meas)

        req = ReqMeasureDirectly(random_basis_local=RandomBasis.XYZ,
                                 random_basis_remote=RandomBasis.XYZ)
        egp.put(request=req)
        yield self.await_signal(sender=egp, signal_label=signal_label_meas)

        req = ReqMeasureDirectly(random_basis_local=RandomBasis.CHSH,
                                 random_basis_remote=RandomBasis.CHSH)
        ack_create_id = egp.put(request=req)
        yield self.await_signal(sender=egp, signal_label=signal_label_error)
        res_error = egp.get_signal_result(signal_label_error, self)

        assert ack_create_id == 5
        assert res_error.create_id == 5
        assert res_error.error_code == ErrorCode.UNSUPP

        req = ReqReceive()
        egp.put(request=req)
        yield self.await_timer(duration=10)

        req = ReqStopReceive()
        egp.put(request=req)
        yield self.await_timer(duration=10)

        self.finished = True


def test_egp_service():
    ns.sim_reset()
    node = Node("test_node")
    egp = SimpleEGP(node=node)
    network_layer = MockNetworkLayer(node=node, egp=egp)
    network_layer.start()
    assert not network_layer.finished
    ns.sim_run()
    assert network_layer.finished

    assert egp.performed_create_and_keep
    assert egp.performed_measure_directly_chsh
    assert egp.performed_measure_directly_fixed
    assert egp.performed_create_and_measure_fixed_no_default_values
    assert egp.performed_measure_directly_xz
    assert egp.performed_measure_directly_xyz
    assert egp.performed_receive
    assert egp.performed_stop_receive
