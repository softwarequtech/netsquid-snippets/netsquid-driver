import inspect
from typing import Type

from netsquid.components.component import Component
from netsquid.nodes import Node
from netsquid.protocols import ServiceProtocol


class Driver(Component):
    """Component providing an access point to a node.
    It provides a lookup between classes of services and respective instantiations. Note service is here used
    as a catch-all term and does not necessarily correspond to a NetSquid Service.

    Parameters
    ----------
    name : str
        Name of the Driver.

    """

    def __init__(self, name):
        super().__init__(name=name)
        self._services = {}

    def add_service(self, service_class: Type[ServiceProtocol], service_instantiation: ServiceProtocol):
        """Add a service class-instantiation pair to the Driver.

        This informs the Driver of a mapping between a class and an instantiation of it.

        Parameters
        ---------
        service_class :
            Class of the service
        service_instantiation :
            Instance of service_class

        """
        if not inspect.isclass(service_class):
            raise TypeError("service_class must be a class.")
        if not isinstance(service_instantiation, service_class):
            raise TypeError("service_instantiation must be an instance of service_class")

        self._services.update({service_class: service_instantiation})

    @property
    def services(self):
        """List of programs known to the Driver"""
        return self._services

    @property
    def is_connected(self):
        """Checks whether the Driver is subcomponent of a node."""
        return isinstance(self.supercomponent, Node)

    def __getitem__(self, service: Type[ServiceProtocol]):
        if self.is_connected:
            return self._services[service]
        else:
            raise Exception("This Driver is not connected to a Node.")

    def start_all_services(self):
        for service_instantiation in self._services.values():
            service_instantiation.start()

    def stop_all_services(self):
        for service_instantiation in self._services.values():
            service_instantiation.stop()
