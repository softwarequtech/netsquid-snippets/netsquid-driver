import unittest
from typing import Dict

import netsquid as ns
from netsquid.nodes import Node
from netsquid.protocols import NodeProtocol
from netsquid_driver.classical_socket_service import ClassicalSocketService
from netsquid_driver.connectionless_socket_service import (
    ConnectionlessSocket,
    ConnectionlessSocketService,
)
from util import create_linear_network_with_routing


class SocketTestListener(NodeProtocol):
    def __init__(self, node: Node, socket: ConnectionlessSocket):
        super().__init__(node)
        self.timestamps = []
        self.messages = []
        self.socket = socket

    def run(self):
        while True:
            msg = yield from self.socket.recv()
            self.timestamps.append(ns.sim_time())
            self.messages.append(msg)


class ClassicalSocketTests(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()
        self.socket_dict: Dict[str, Dict[str, ConnectionlessSocket]] = {}
        self.node_dict: Dict[str, Node] = {}
        self.socket_service_dict: Dict[str, ConnectionlessSocketService] = {}

    def create_network(self, node_names, delay=0.):
        nodes = create_linear_network_with_routing(node_names, connection_delay=delay)
        for node in nodes:
            self.node_dict[node.name] = node
            socket_service = ConnectionlessSocketService(node)
            node.driver.add_service(ClassicalSocketService, socket_service)
            self.socket_service_dict[node.name] = socket_service

    def test_two_nodes_message(self):
        node_names = ["Node0", "Node1"]
        msg = "test"
        self.create_network(node_names)

        for node in self.node_dict.values():
            node.driver.start_all_services()

        send_socket = self.socket_service_dict["Node0"].create_socket()
        recv_socket = self.socket_service_dict["Node1"].create_socket()

        recv_socket.bind("0", "Node0")
        send_socket.send_to(msg, "0", "Node1")

        ns.sim_run()

        self.assertEqual(len(recv_socket._buffer), 1)
        recv_msg = recv_socket._buffer[0].payload
        self.assertEqual(msg, recv_msg)

    def test_two_nodes_modify_msg(self):
        node_names = ["Node0", "Node1"]
        msg = "original message"
        self.create_network(node_names)

        for node in self.node_dict.values():
            node.driver.start_all_services()

        send_socket = self.socket_service_dict["Node0"].create_socket()
        recv_socket = self.socket_service_dict["Node1"].create_socket()

        recv_socket.bind("0", "Node0")
        send_socket.send_to(msg, "0", "Node1")

        ns.sim_run()
        # Modify original object
        msg += "_alteration"

        self.assertEqual(len(recv_socket._buffer), 1)
        recv_msg = recv_socket._buffer[0].payload
        self.assertEqual("original message", recv_msg)

    def test_multiple_nodes_multiple_message(self):
        node_names = ["Node0", "Node1", "Node2", "Node3", "Node4"]
        msg = "test"
        self.create_network(node_names)

        for node in self.node_dict.values():
            node.driver.start_all_services()

        socket0_4 = self.socket_service_dict["Node0"].create_socket()
        socket0_2 = self.socket_service_dict["Node0"].create_socket()

        socket0_4.bind("0", "Node4")
        socket0_2.bind("0", "Node2")

        socket4_2 = self.socket_service_dict["Node4"].create_socket()
        socket4_0 = self.socket_service_dict["Node4"].create_socket()
        socket2 = self.socket_service_dict["Node2"].create_socket()

        socket4_2.bind("0", "Node2")
        socket4_0.bind("0", "Node0")

        # 2 messages from 0 to 4, one from 4 to 0 and one from 2 to 4
        socket0_4.send_to(msg, "0", "Node4")
        socket0_4.send_to(msg, "0", "Node4")
        socket4_0.send_to(msg, "0", "Node0")
        socket2.send_to(msg, "0", "Node4")

        ns.sim_run()

        self.assertEqual(len(socket4_0._buffer), 2)
        self.assertEqual(len(socket4_2._buffer), 1)
        self.assertEqual(socket4_0._buffer[0].payload, msg)
        self.assertEqual(socket4_0._buffer[1].payload, msg)
        self.assertEqual(socket4_2._buffer[0].payload, msg)

        self.assertEqual(len(socket0_4._buffer), 1)
        self.assertEqual(socket0_4._buffer[0].payload, msg)

        self.assertEqual(len(socket2._buffer), 0)
        self.assertEqual(len(socket0_2._buffer), 0)

    def test_message_order_and_arrival_time(self):
        node_names = ["Node0", "Node1", "Node2", "Node3", "Node4", "Node5"]
        one_link_delay = 12.5
        num_messages = 7
        self.create_network(node_names, one_link_delay)
        expected_delay = one_link_delay * (len(node_names) - 1)

        send_socket = self.socket_service_dict["Node0"].create_socket()
        recv_socket = self.socket_service_dict["Node5"].create_socket()

        recv_socket.bind("0")

        listen_protocol = SocketTestListener(self.node_dict["Node5"], recv_socket)

        listen_protocol.start()
        for node in self.node_dict.values():
            node.driver.start_all_services()

        for i in range(num_messages):
            send_socket.send_to(str(i), "0", "Node5")

        ns.sim_run()

        self.assertEqual(len(recv_socket._buffer), 0)
        self.assertEqual(len(listen_protocol.messages), num_messages)
        for i in range(num_messages):
            self.assertEqual(listen_protocol.messages[i], str(i))
            self.assertAlmostEqual(listen_protocol.timestamps[i], expected_delay)

    def test_connect_sockets(self):
        node_names = ["Node0", "Node1", "Node2"]
        msg = "test"
        self.create_network(node_names)

        for node in self.node_dict.values():
            node.driver.start_all_services()

        socket0 = self.socket_service_dict["Node0"].create_socket()
        socket2 = self.socket_service_dict["Node2"].create_socket()

        socket0.bind("0", "Node2")
        socket0.connect("0", "Node2")

        socket2.bind("0", "Node0")
        socket2.connect("0", "Node0")

        socket0.send(msg)
        socket0.send(msg)

        ns.sim_run()

        self.assertEqual(len(socket0._buffer), 0)
        self.assertEqual(len(socket2._buffer), 2)
        recv_msg = socket2._buffer[0].payload
        self.assertEqual(msg, recv_msg)

        socket2.send(msg)
        socket2.send(msg)

        ns.sim_run()

        self.assertEqual(len(socket0._buffer), 2)
        recv_msg = socket0._buffer[0].payload
        self.assertEqual(msg, recv_msg)

    def test_multiple_ports(self):
        node_names = ["Node0", "Node1", "Node2"]
        msg_for_a = "special msg a"
        msg_for_b = "special msg b"

        self.create_network(node_names)

        for node in self.node_dict.values():
            node.driver.start_all_services()

        socket0_a = self.socket_service_dict["Node0"].create_socket()
        socket0_b = self.socket_service_dict["Node0"].create_socket()

        socket2_a = self.socket_service_dict["Node2"].create_socket()
        socket2_b = self.socket_service_dict["Node2"].create_socket()

        socket0_a.bind("a", None)
        socket0_b.bind("b", None)

        socket2_a.bind("a", None)
        socket2_b.bind("b", None)

        socket0_a.send_to(msg_for_a, "a", "Node2")
        socket0_b.send_to(msg_for_a, "a", "Node2")

        ns.sim_run()

        self.assertEqual(len(socket2_a._buffer), 2)
        self.assertEqual(len(socket2_b._buffer), 0)
        recv_msg = socket2_a._buffer[0]
        self.assertEqual(recv_msg.payload, msg_for_a)
        self.assertEqual(recv_msg.origin.node_name, "Node0")

        self.assertIn(recv_msg.origin.port_name, ["a", "b"])

        socket0_a.send_to(msg_for_b, "b", "Node2")
        socket0_b.send_to(msg_for_b, "b", "Node2")

        ns.sim_run()

        self.assertEqual(len(socket2_b._buffer), 2)

    def test_close(self):
        node_names = ["Node0", "Node1", "Node2"]
        msg = "test"
        self.create_network(node_names)

        for node in self.node_dict.values():
            node.driver.start_all_services()

        socket0_0 = self.socket_service_dict["Node0"].create_socket()

        socket2 = self.socket_service_dict["Node2"].create_socket()

        socket0_0.bind("0", None)
        socket2.send_to(msg, "0", "Node0")

        ns.sim_run()

        self.assertEqual(len(socket0_0._buffer), 1)

        socket0_1 = self.socket_service_dict["Node0"].create_socket()

        with self.assertRaises(RuntimeError):
            socket0_1.bind("0", "Node2")

        # The previous statement will have socket0_1 bound
        socket0_1.close()

        socket0_0.close()
        socket0_1.bind("0", "Node2")

        socket2.send_to(msg, "0", "Node0")

        ns.sim_run()

        self.assertEqual(len(socket0_1._buffer), 1)
