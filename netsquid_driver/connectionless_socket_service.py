from __future__ import annotations

import logging
from dataclasses import dataclass
from typing import Any, Dict, Generator, List, Optional

from netsquid.protocols import Protocol
from netsquid_driver.classical_routing_service import ClassicalRoutingService, RemoteServiceRequest
from netsquid_driver.classical_socket_service import (
    ClassicalSocket,
    ClassicalSocketService,
    SocketAddress,
)
from pydynaa import EventExpression

logger = logging.getLogger(__name__)


@dataclass
class ReqSendSocketMessage:
    """Request to send a message to a remote node."""
    origin: SocketAddress
    target: SocketAddress
    payload: Any


@dataclass
class ReqDeliverSocketMessage:
    """Request to deliver the message to a socket."""
    origin: SocketAddress
    target: SocketAddress
    payload: Any


@dataclass
class ResSocketMessage:
    """Result of a message arriving to a socket."""
    origin: SocketAddress
    target: SocketAddress
    payload: Any


class ConnectionlessSocket(ClassicalSocket, Protocol):
    def __init__(
        self,
        socket_service: ConnectionlessSocketService,
    ):
        """
        A connectionless socket that behaves like a UDP socket.

        To receive messages the socket must be bound first.
        Sending via the :meth:`send_to` methods is always possible,
        but to use :meth:`send` the socket must be connected first.

        :param socket_service: The service managing this socket.
        """
        super().__init__()
        self.socket_service = socket_service
        self.address = SocketAddress(self.node_name, port_name="temp")
        self.remote_address: Optional[SocketAddress] = None
        self._bound_remote_node: Optional[str] = None
        self._buffer: List[ResSocketMessage] = []
        self._msg_arrive_signal_label: Optional[str] = None

    def bind(self, port_name: str, remote_node_name: str = None):
        """Binds the socket to a port and optionally to a remote node.

        :param port_name: The name of the port to bind to.
        :param remote_node_name: The name of the remote node to bind to.
         If not specified or set to None, it will receive from all remote nodes.
        """
        self.address.port_name = port_name
        self._bound_remote_node = remote_node_name
        self.socket_service.bind(self, port_name, remote_node_name)

    def connect(self, remote_port_name: str, remote_node_name: str):
        """Connects the socket to a remote port and node.

        :param remote_port_name: The name of the remote port to connect to.
        :param remote_node_name: The name of the remote node to connect to.
        """
        self.remote_address = SocketAddress(remote_node_name, remote_port_name)

    def send(self, msg: str):
        """Sends a string message to a connected remote socket.

        :param msg: The message to send.
        """
        if not self.remote_address:
            raise RuntimeError(f"Socket: {self.address} not connected.")
        self.send_to(msg, self.remote_address.port_name, self.remote_address.node_name)

    def send_to(self, msg: str, remote_port_name: str, remote_node_name: str) -> None:
        """Sends a message to the remote port and node.

        :param msg: The message to send.
        :param remote_port_name: The name of the remote port to send to.
        :param remote_node_name: The name of the remote node to send to.
        """
        target = SocketAddress(remote_node_name, remote_port_name)
        req = ReqSendSocketMessage(target=target, payload=msg, origin=self.address)
        self.socket_service.put(req)

    def _push_message_payload(self, message: ResSocketMessage):
        assert message.target.port_name == self.address.port_name
        assert message.target.node_name == self.address.node_name
        self._buffer.append(message)

    def recv(self) -> Generator[EventExpression, None, str]:
        """Wait and receives a string message.

        :return: A generator that yields an event expression and returns a string message.
        """
        # The while loop is required because it can happen that if there are two recv calls behind each other
        # the second recv may use the signal of the first to "skip" the waiting process
        while len(self._buffer) == 0:
            yield self.await_signal(sender=self.socket_service, signal_label=self.address.port_name)
        return self._buffer.pop(0).payload

    def recv_from(self) -> Generator[EventExpression, None, (str, SocketAddress)]:
        """Wait and receive a string message and return both the message and the address it was sent from.

        :return: A generator that yields an event expression and returns a tuple containing the string message
         and the sender's socket address.
        """
        while len(self._buffer) == 0:
            yield self.await_signal(sender=self.socket_service, signal_label=self.address.port_name)
        msg = self._buffer.pop(0)
        return msg.payload, msg.origin

    @property
    def node_name(self) -> str:
        return self.socket_service.node.name

    def close(self):
        """Closes the socket."""
        self.socket_service.unbind(self.address.port_name, self._bound_remote_node)


class ConnectionlessSocketService(ClassicalSocketService):

    def __init__(self, node, name=None):
        """Initializes the connectionless socket service.

        This service is required to supports the sockets: :py:class:`ConnectionlessSocket`.

        :param node: The node associated with this service.
        :param name: The name of the service (optional).
        """
        super().__init__(node=node, name=name)
        self.register_request(ReqSendSocketMessage, self.send_message)
        self.register_request(ReqDeliverSocketMessage, self.handle_message)
        self._bound_sockets: Dict[(str, str), ConnectionlessSocket] = {}
        self.logger = logger.getChild(self.node.name)

    def bind(self, socket: ConnectionlessSocket, port_name: str, remote_node_name: str = None):
        """Binds a socket to a port and optionally to a remote node.

        :param socket: The connectionless socket to bind.
        :param port_name: The name of the port to bind to.
        :param remote_node_name: The name of the remote node to bind to (optional).
        If None, the socket will be bound to all nodes. (i.e., with no specific remote)
        :raises RuntimeError: If there is a conflict with bound sockets.
        """
        if port_name not in self.response_types:
            self.register_response(ResSocketMessage, name=port_name)

        socket_key = (port_name, remote_node_name)

        if socket_key in self._bound_sockets.keys():
            raise RuntimeError(f"A socket with port, remote node: {socket_key} is already bound.")

        self._bound_sockets[socket_key] = socket
        self._check_port_socket_conflict(port_name)

    def unbind(self, port_name: str, remote_node_name: str):
        """Unbinds a socket.

        :param port_name: The name of the port to unbind from.
        :param remote_node_name: The name of the remote node to unbind from.
        :raises RuntimeError: If the socket is not bound.
        """
        key = (port_name, remote_node_name)
        if key in self._bound_sockets.keys():
            self._bound_sockets.pop(key)
        else:
            raise RuntimeError("Trying to unbind a socket that is not bound.")

    def create_socket(self) -> ConnectionlessSocket:
        """Creates a new connectionless socket.

        :return: An instance of a ConnectionlessSocket.
        """
        return ConnectionlessSocket(self)

    def send_message(self, req: ReqSendSocketMessage):
        """Sends a message via the ClassicalMessageService.

        :param req: The request to send a message.
        """
        message = RemoteServiceRequest(request=ReqDeliverSocketMessage(req.origin, req.target, req.payload),
                                       service=ClassicalSocketService,
                                       origin=self.node.name,
                                       targets=[req.target.node_name])

        self.node.driver.services[ClassicalRoutingService].put(message)

    def handle_message(self, req: ReqDeliverSocketMessage):
        """Handles incoming messages and routes them to the appropriate socket.

        :param req: The request containing the message to handle.
        """
        res = ResSocketMessage(req.origin, req.target, req.payload)
        key_1 = (req.target.port_name, None)
        key_2 = (req.target.port_name, req.origin.node_name)

        # We push the message into the buffer "manually" instead of using the signal to send the message to the socket,
        # as signals and their contents are overwritten and that can cause issues for messages arriving at same time
        if key_1 in self._bound_sockets.keys():
            self._bound_sockets[key_1]._push_message_payload(res)
            self.send_response(res, name=req.target.port_name)
        elif key_2 in self._bound_sockets.keys():
            self._bound_sockets[key_2]._push_message_payload(res)
            self.send_response(res, name=req.target.port_name)
        else:
            self.logger.warning(f"No socket on port {req.target.port_name} available to receive message: {req}")

    def _check_port_socket_conflict(self, port_name: str):
        """
        Checks for conflicts in socket bindings on the specified port.

        This method verifies that no socket is bound to both all nodes (i.e., with no specific remote)
        and a specific node on the same port. If such a conflict is found, it raises a RuntimeError.
        """
        if (port_name, None) not in self._bound_sockets.keys():
            return

        for key in self._bound_sockets.keys():
            if key[0] is port_name and key[1] is not None:
                raise RuntimeError(f"Conflict with bound sockets on port {port_name}")
