from abc import ABCMeta
from collections import deque
from dataclasses import asdict

import netsquid as ns
from netsquid.protocols import ServiceProtocol


class ServiceWithQueue(ServiceProtocol, metaclass=ABCMeta):
    """Abstract base class providing queueing mechanism for ServiceProtocols.

    Ensures that requests are handled in a FIFO order.
    """
    def __init__(self, node, name="DEJMPSAbstract"):
        super().__init__(node=node, name=name)
        self.queue = deque()
        self._new_req_signal = "New request in queue"
        self.add_signal(self._new_req_signal)

    def run(self):
        """Wait for a new request signal, then run the requests one by one.

        Assumes request handlers are generators and not functions.

        """
        while True:
            yield self.await_signal(self, self._new_req_signal)
            while len(self.queue) > 0:
                start_time, (handler_id, request, kwargs) = self.queue.popleft()
                if start_time > ns.sim_time():
                    yield self.await_timer(end_time=start_time)
                func = self.request_handlers[handler_id]
                args = asdict(request)
                gen = func(**{**args, **kwargs})
                yield from gen

    def handle_request(self, request, identifier, start_time=None, **kwargs):
        """Schedule the request on the queue.

        Schedule the request in a queue and signal that new items have been put into the queue.

        Parameters
        ----------
        request :
            The object representing the request.
        identifier : str
            The identifier for this request.
        start_time : float, optional
            The time at which the request can be executed. Default current simulation time.
        kwargs : dict, optional
            Additional arguments which can be set by the service.

        Returns
        -------
        dict
            The dictionary with additional arguments.

        Notes
        -----
        This method is called after
        :meth:`~netsquid.protocols.serviceprotocol.ServiceProtocol.put` which
        does the type checking etc.

        """

        if start_time is None:
            start_time = ns.sim_time()
        self.queue.append((start_time, (identifier, request, kwargs)))
        self.send_signal(self._new_req_signal)
        return kwargs
