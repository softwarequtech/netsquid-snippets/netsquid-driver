import netsquid as ns
import netsquid_driver.distillation as distilservice
from netsquid.nodes import Node
from netsquid.protocols import NodeProtocol


class SimpleDistillation(distilservice.DistillationService):

    def __init__(self, node):
        super().__init__(node=node, name="simple_distillation")
        self.performed_attempt = False
        self.performed_evaluate_outcome = False
        self.id = 1

    def start(self):
        super().start()

    def stop(self):
        super().stop()

    def attempt(self, qubit_indices, distillation_parameter=0):
        self.performed_attempt = True
        assert qubit_indices == [0, 1]
        dummy_measurement_outcome = [0]
        yield self.await_timer(end_time=0.1)
        self.send_response(distilservice.ResCircuitSuccess(measurement_outcome=dummy_measurement_outcome))

    def evaluate_outcome(self, measurement_outcomes):
        self.performed_evaluate_outcome = True
        assert measurement_outcomes == [0, 0]
        outcome = True
        self.send_response(distilservice.ResEvaluateProtocolOutcome(protocol_outcome=outcome))
        yield self.await_signal(self)


class MockNetworkLayer(NodeProtocol):

    def __init__(self, node, distil):
        super().__init__(node=node, name="mock_network_layer")
        self.add_subprotocol(distil, "Distil")
        self.finished = False

    def start(self):
        super().start()

    def stop(self):
        super().stop()

    def run(self):

        distil = self.subprotocols["Distil"]
        distil.start()

        signal_label_succ = distilservice.ResCircuitSuccess.__name__
        signal_label_outc = distilservice.ResEvaluateProtocolOutcome.__name__

        qubit_indices = [0, 1]

        req = distilservice.ReqDistillationAttempt(qubit_indices=qubit_indices)
        distil.put(request=req)
        yield self.await_signal(sender=distil, signal_label=signal_label_succ)
        res_succ = distil.get_signal_result(label=signal_label_succ, receiver=self)

        dummy_measurement_outcomes = [0, 0]

        req = distilservice.ReqDistillationOutcome(measurement_outcomes=dummy_measurement_outcomes)
        distil.put(request=req)
        yield self.await_signal(sender=distil, signal_label=signal_label_outc)
        res_outc = distil.get_signal_result(label=signal_label_outc, receiver=self)

        assert res_succ.measurement_outcome == [0]
        assert res_outc.protocol_outcome is True
        self.finished = True


def test_distillation_service():
    ns.sim_reset()
    node = Node("test_node")
    distil = SimpleDistillation(node=node)
    network_layer = MockNetworkLayer(node=node, distil=distil)
    network_layer.start()
    assert not network_layer.finished
    ns.sim_run()
    assert network_layer.finished

    assert distil.performed_attempt
    assert distil.performed_evaluate_outcome
