from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from enum import IntEnum

from netsquid.protocols.serviceprotocol import ServiceProtocol
from netsquid.qubits.ketstates import BellIndex
from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService


@dataclass
class ReqEntanglement:
    """Request generation of entanglement using a specific port.

    Will perform repeated attempts at entanglement generation until success is achieved.

    """
    remote_node_name: str  # Name of remote node that should be used to generate entanglement.
    mem_pos: int = 0  # Memory position where entanglement should be stored.


@dataclass
class ReqEntanglementAbort:
    """Stop attempting to generate entanglement."""
    remote_node_name: str  # Name of remote node that should no longer be used for entanglement generation.


@dataclass
class ResEntanglementSuccess:
    """Response indicating that entanglement has been generated successfully.

    It is not guaranteed that the memory position indicated by `mem_pos` still holds the entangled qubit that has
    been successfully created. It is only guaranteed that the qubit was present at this memory position
    at the moment the last a :class:`ResStateReady` response was sent.

    """
    remote_node_id: int  # :prop:`netsquid.nodes.node.Node.ID` of the node that entanglement has been generated with.
    remote_node_name: str  # Name of remote node that has been used to generate entanglement.
    mem_pos: int = 0  # Memory position that held the entangled qubit directly after it was locally available.
    bell_index: BellIndex = BellIndex(0)  # Bell index indicating which Bell state was created.


@dataclass
class ResEntanglementFail:
    """Response indicating that the latest attempt at entanglement generation has failed.

    Note that, unless entanglement generation is aborted, a new attempt will follow directly after each failure.

    """
    remote_node_name: str  # Name of remote node that was used to attempt entanglement generation.
    mem_pos: int = 0  # Memory position that was used to attempt entanglement generation.


@dataclass
class ResStateReady:
    """Response indicating that a local qubit has become available during an attempt of entanglement generation.

    When a local qubit becomes available, it may not yet be known whether the attempt at entanglement generation
    has been successful.
    Whether the state is actually entangled (or will be entangled) or not, will only be known when a
    :class:`ResEntanglementSuccess` or :class:`ResEntanglementFail` is sent.
    When this response is sent, the state can already be acted on. It can e.g. be measured or moved to a storage qubit.

    """
    remote_node_name: str  # Name of remote node that was used to attempt entanglement generation.
    mem_pos: int = 0  # Memory position where the state has become available.


class EntanglementError(IntEnum):
    """Error codes for :class:`EntanglementService`."""
    ABORTED = 0  # this error is raised when entanglement has been aborted through a ReqEntanglementAbort


@dataclass
class ResEntanglementError:
    """Response indicating that an error has occurred during entanglement generation."""
    error_code: EntanglementError  # Can be used to identify the error that occurred.
    remote_node_name: str  # Name of the port on which entanglement was being generated when error occurred.
    mem_pos: int = 0  # Memory position at which entanglement was being generated when error occurred.


class EntanglementService(ServiceProtocol, metaclass=ABCMeta):
    """Service that repeats attempts at entanglement generation until it is successful.

    Entanglement-generation attempts are started when a :class:`ReqEntanglement` request
    is put at the local :class:`EntanglementService` at two different nodes.
    Each time a qubit that the service tries to entangle becomes available, a :class:`ResStateReady` response
    is sent by the service.
    Whether the attempt was successful (and thus whether the qubit is entangled or not) is indicated by either a
    :class:`ResEntanglementFail` or :class:`ResEntanglementSuccess` response.
    When a :class:`ResEntanglementSuccess` response is sent, the request that triggered the attempts is considered
    completed.
    A :class:`ReqEntanglementAbort` can be used to stop all entanglement generation on a specific port (this includes
    requests that are queued but not yet handled). A :class:`ResEntanglementError` response is sent when the abortion
    is concluded, so that other nodes learn that entanglement generation attempts are no longer underway.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    name : str, optional
        The name of this protocol.

    """
    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self.register_request(req_type=ReqEntanglement, handler=self.generate_entanglement)
        self.register_request(req_type=ReqEntanglementAbort, handler=self.abort)
        self.register_response(res_type=ResEntanglementSuccess)
        self.register_response(res_type=ResEntanglementFail)
        self.register_response(res_type=ResStateReady)
        self.register_response(res_type=ResEntanglementError)

    @abstractmethod
    def generate_entanglement(self, req):
        """Generate entanglement successfully once.

        Parameters
        ----------
        req : :class:`ReqEntanglement`
            Request that needs to be handled by this method.

        Note
        ----
        Should call :meth:`entanglement_service.EntanglementService.send_response(res)` where res is...
        :class:`ResStateReady` everytime when a qubit that partakes in entanglement generation becomes available,
        :class:`ResEntanglementFail` everytime when an attempt at entanglement generation fails,
        :class:`ResEntanglementSuccess` everytime when an attempt at entanglement generation is confirmed to have
        been successful (thereby fulfilling the request),
        :class:`ResEntanglementError` everytime when an error occurs.

        When an attempt at entanglement generation is successful, this should also be registered at the local
        entanglement tracker, if there is any.
        This is done automatically whenever a :class:`ResEntanglementSuccess` is sent using
        :meth:`EntanglementService.send_response`.

        """
        assert isinstance(req, ReqEntanglement)

    @abstractmethod
    def abort(self, req):
        """Abort entanglement generation.

        Parameters
        ----------
        req : :class:`ReqEntanglementAbort`
            Request that needs to be handled by this method.

        """
        assert isinstance(req, ReqEntanglementAbort)

    def send_response(self, response, name=None):
        """Send a response via a signal.

        If the response is a :class:`ResEntanglementSuccess`,
        the generated entanglement is automatically registered at the local entanglement tracker
        (if there is any).

        Parameters
        ----------
        response : :class:`collections.namedtuple` or object
            The response instance.
        name : str or None, optional
            The identifier used for this response.
            Default :meth:`~netsquid.protocols.serviceprotocol.ServiceProtocol.get_name` of the request.

        Raises
        ------
        ServiceError
            If the name doesn't match to the request type.

        """
        if isinstance(response, ResEntanglementSuccess) and self._entanglement_tracker is not None:
            self._entanglement_tracker.register_new_local_link(remote_node_id=response.remote_node_id,
                                                               bell_index=response.bell_index,
                                                               mem_pos=response.mem_pos,
                                                               time_of_birth=None)
        super().send_response(response=response, name=name)

    @property
    def _entanglement_tracker(self):
        """The local entanglement tracker, if there is any.

        Returns
        -------
        :class:`entanglement_tracker_service.EntanglementTrackerService` or None
            Entanglement tracker if there is any, otherwise None.

        """
        entanglement_tracker = None
        try:
            entanglement_tracker = self.node.driver[EntanglementTrackerService]
        except (AttributeError, KeyError):
            pass
        return entanglement_tracker
