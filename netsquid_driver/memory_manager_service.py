from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Optional

from netsquid.protocols.serviceprotocol import ServiceProtocol
from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService


@dataclass
class ReqMove:
    """Request to move a state.

    Parameters
    ----------
    from_memory_position_id : int
        Index of memory position from where state should be moved.
    to_memory_position_id : int or None (optional)
        Index of memory position to where state should be moved. If None, an arbitrary free position will be chosen.

    """
    from_memory_position_id: int
    to_memory_position_id: Optional[int] = None


@dataclass
class ReqFreeMemory:
    """Request to free-up memory.

    If no memory position ID is given, all memory positions are freed.

    Parameters
    ----------
    memory_position_id : list or None (optional)
        List of indices to free. If None, all positions are freed.

    """
    memory_position_id: Optional[list] = None


@dataclass
class ResMoveSuccess:
    """Response indicating a successful move.

    Parameters
    ----------
    memory_position_id : int
        Index of memory position to which state was moved.

    """
    memory_position_id: int


class QuantumMemoryManager(ServiceProtocol, metaclass=ABCMeta):
    """Service framework for a quantum memory manager.

    If parameter `size` is not specified, the number of positions in the node's quantum memory, `num_positions` is used.
    Otherwise, access of the memory manager to the quantum memory's positions is limited to positions in the range 0 to
    `size`. This can be useful if e.g. the quantum memory has a position that is meant for emission, but not for quantum
    information storage and processing.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on. Must have a quantum memory object as subcomponent.
    size : int, optional
        Number of positions in this memory.
    name : str, optional
        The name of this protocol.

    """

    def __init__(self, node, size=None, name=None):
        super().__init__(node=node, name=name)
        if size is None:
            self.size = node.qmemory.num_positions
        else:
            self.size = size
        self.register_request(req_type=ReqMove, handler=self.handle_req_move)
        self.register_request(req_type=ReqFreeMemory, handler=self.handle_req_free_memory)
        self.register_response(ResMoveSuccess)

    def move(self, from_memory_position_id, to_memory_position_id=None):
        """Moves a qubit from one position to the other.

        If the target position is `None`, moves to an arbitrary free position.

        Parameters
        ----------
        from_memory_position_id : int
            Index of memory position from where state should be moved.
        to_memory_position_id : int or None (optional)
            Index of memory position to where state should be moved. If None, an arbitrary free position will be chosen.

        """
        if not isinstance(from_memory_position_id, int):
            raise TypeError(f"{from_memory_position_id} is not an integer.")
        if from_memory_position_id < 0:
            raise ValueError(f"Invalid memory position value {from_memory_position_id}.")
        if not isinstance(to_memory_position_id, int) and to_memory_position_id is not None:
            raise TypeError(f"{to_memory_position_id} is not an integer or None.")
        if isinstance(to_memory_position_id, int) and to_memory_position_id < 0:
            raise ValueError(f"Invalid memory position value {to_memory_position_id}.")
        if to_memory_position_id is None:
            to_memory_position_id = self._get_free_pos()
        if to_memory_position_id == -1:
            raise ValueError("No free position available.")
        self.set_position_in_use_by_id(to_memory_position_id)
        self.free_position(from_memory_position_id)
        if EntanglementTrackerService in self.node.driver.services:
            self.node.driver[EntanglementTrackerService].register_move(from_memory_position_id, to_memory_position_id)
        self.send_response(ResMoveSuccess(memory_position_id=to_memory_position_id))

    def handle_req_move(self, req):
        """Handle request to move a state.

        Calls the method :meth:`move` using the parameters given in the request.

        Parameters
        ----------
        req : :class:`ReqMove`
            Request to move state.

        """
        assert isinstance(req, ReqMove)
        self.move(from_memory_position_id=req.from_memory_position_id,
                  to_memory_position_id=req.to_memory_position_id)

    def free_memory(self, memory_position_id):
        """Frees up memory position.

        If no memory position is specified (i.e. `memory_position_id` is `None`), all memory positions are freed.

        Parameters
        ----------
        memory_position_id : list or None (optional)
            Indices of memory positions to be freed. If None, all positions are freed

        """
        if type(memory_position_id) is int:
            memory_position_id = [memory_position_id]

        if not isinstance(memory_position_id, list) and memory_position_id is not None:
            raise TypeError(f"{memory_position_id} is not a list or None.")

        if memory_position_id is None:
            self.free_all_positions()
        else:
            for qubit_id in memory_position_id:
                self.free_position(qubit_id)

    def handle_req_free_memory(self, req):
        """Handle request to free-up memory.

        Calls the method :meth:`free_memory` using the parameters given in the request.

        Parameters
        ----------
        req : :class:`ReqFreeMemory`
            Request to free up memory.

        """
        assert isinstance(req, ReqFreeMemory)
        if isinstance(req.memory_position_id, int):
            req.memory_position_id = [req.memory_position_id]
        self.free_memory(memory_position_id=req.memory_position_id)

    @property
    def size(self):
        """
        Returns
        -------
        int
            Size of quantum memory.

        """
        return self._size

    @size.setter
    def size(self, value):
        if not isinstance(value, int):
            raise TypeError(f"{value} is not an integer.")
        if value < 1:
            raise ValueError(f"{value} is an invalid memory size.")
        self._size = value

    def set_position_in_use_by_id(self, index):
        """Mark a particular position as in use.

        Parameters
        ----------
        index : int
            The memory position index to set as in use.

        """
        if index >= self.size:
            raise IndexError("Memory position {} out of range!".format(index))

        self.node.qmemory.mem_positions[index].in_use = True

    def free_all_positions(self):
        """Frees all positions in the quantum memory.

        """
        for index in range(self.size):
            self.free_position(index)

    @abstractmethod
    def free_position(self, index):
        """Frees a position.

        Parameters
        ----------
        index : int
            The memory position index to free.
        """
        if index >= self.size:
            raise IndexError("Memory position {} out of range!".format(index))

    def _get_free_pos(self):
        """Gets the index of a free memory position. If there are no free positions, -1 is returned.
        """
        free_pos_indices = [pos for pos in range(self.size) if not self.node.qmemory.mem_positions[pos].in_use]
        try:
            pos_id = free_pos_indices[0]
            return pos_id

        except IndexError:
            return -1

    @staticmethod
    def _position_to_index(position):
        """Gets the index of a given memory position.

        Parameters
        ----------
        position :class:`netsquid.components.qmemory.MemoryPosition`
            Memory position for which to get index.
        """
        return int(position.name.split('position')[1])
