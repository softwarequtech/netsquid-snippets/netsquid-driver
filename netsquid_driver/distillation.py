from abc import ABCMeta, abstractmethod
from dataclasses import dataclass

from netsquid_driver.service_with_queue import ServiceWithQueue


@dataclass
class ReqDistillationAttempt:
    """Class for distillation attempt requests.

    Upon receiving this request, the distillation service will execute the local distillation circuit and send a success
    or failure response in accordance with the outcome of the circuit.

    Parameters
    ----------
    qubit_indices : list
        List containing the indices of the qubits on which to perform the distillation circuit
    distillation_parameter : float, optional
        Extra parameter required by certain distillation circuits

    """
    qubit_indices: list
    distillation_parameter: float = 0


@dataclass
class ReqDistillationOutcome:
    """Class for distillation outcome requests.

    Upon receiving this request, the distillation service will decide based on the received measurement outcomes whether
    distillation was successful or not.

    Parameters
    ----------
    measurement_outcomes : list
        List containing the relevant measurement outcomes from both the local node and remote node(s).

    """
    measurement_outcomes: list


@dataclass
class ResCircuitSuccess:
    """Class for successful local circuit execution.

    The distillation service sends this response if the local distillation circuit ran successfully.

    Parameters
    ----------
    measurement_outcome : list
        List containing the outcome of the measurements done in the context of the distillation circuit.

    """
    measurement_outcome: list


@dataclass
class ResCircuitFailed:
    """Class for failed local circuit execution.

    The distillation service sends this response if the local distillation circuit failed.

    Parameters
    ----------
    error_code : int
        Integer identifying the type of error that occurred.

    """
    error_code: int


@dataclass
class ResEvaluateProtocolOutcome:
    """Class for distillation outcome responses.

    Parameters
    ----------
    protocol_outcome : bool
        If True (False), the protocol was (not) successful.

    """
    protocol_outcome: bool


class DistillationService(ServiceWithQueue, metaclass=ABCMeta):
    """Abstract base class from which all distillation services should be subclassed.

    To create your distillation service protocol, overwrite the given abstract methods with the desired functionality
    and ensure that the proper responses are sent.
    No entanglement identifiers are generated, kept or updated in the context of this distillation service. It is
    assumed that whatever entity calls this service is aware of such identifiers and is tasked with tracking them.

    A distillation protocol is started when some entity, such as a Network Layer, places a ReqDistillationAttempt on two
    or more nodes. The nodes then attempt to run a local circuit, which is both protocol and hardware dependent. If the
    circuit terminates with success (failure), they send back a ResCircuitSuccess (ResCircuitFailed) response. Assuming
    that all local circuits terminated succesfully, the entity that started the distillation protocol collects the
    circuit outcomes and sends them in a ReqDistillationOutcome request to one of the nodes. The node can then compare
    all outcomes and decide whether the distillation protocol succeeded. This decision is made in a protocol-dependent
    manner and is sent back in a ResEvaluateProtocolOutcome response.

    Parameters
    ----------
    node : :obj:`~netsquid.nodes.node.Node`
        The node for which the driver provides an interface.
    name : str, optional
        Name of the driver.

    """

    def __init__(self, node, name="Distillation"):
        super().__init__(node=node, name=name)
        self.register_request(req_type=ReqDistillationAttempt, handler=self.attempt)
        self.register_request(req_type=ReqDistillationOutcome, handler=self.evaluate_outcome)
        self.register_response(res_type=ResCircuitSuccess)
        self.register_response(res_type=ResCircuitFailed)
        self.register_response(res_type=ResEvaluateProtocolOutcome)

    @abstractmethod
    def attempt(self, req):
        """Attempt local distillation circuit.

        Parameters
        ----------
        req : :class:`ReqDistillationAttempt`
            Request to be handled by this method.

        Note
        ----

        Should call self.send_response(res).
        When successful, res should be :class:`ResCircuitSuccess`.
        When unsuccessful, res should be :class:`ResCircuitFailed`.

        """
        assert isinstance(req, ReqDistillationAttempt)

    @abstractmethod
    def evaluate_outcome(self, req):
        """Evaluate outcome of distillation protocol.

        Parameters
        ----------
        req : :class:ReqDistillationOutcome`
            Request to be handled by this method.

        Note
        ----

        Should call self.send_response(res) and res should be :class:`ResEvaluateProtocolOutcome`.

        """
        assert isinstance(req, ReqDistillationOutcome)
