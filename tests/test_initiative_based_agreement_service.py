from __future__ import annotations

import unittest
from typing import List, Union

import netsquid as ns
from netsquid.nodes import Node
from netsquid.protocols import NodeProtocol
from netsquid_driver.entanglement_agreement_service import (
    EntanglementAgreementService,
    ReqEntanglementAgreement,
    ReqEntanglementAgreementAbort,
    ResEntanglementAgreementReached,
    ResEntanglementAgreementRejected,
    signal_entanglement_agreement_status_change,
)
from netsquid_driver.initiative_based_agreement_service import (
    InitiativeAgreementService,
    InitiativeBasedAgreementServiceMode,
)
from util import create_linear_network_with_routing


class InitiativeBasedAgreementTests(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()

    def agreement_test(
            self, connection_delay: float, termination_delay: float, send_time_initiator: float,
            send_time_responder: float
    ):
        """
         Perform a test to check that entanglement agreement is established between two nodes.

         :param connection_delay: The classical communication delay between the nodes.
         :param termination_delay: The delay before terminating the agreement test.
         (prevents subprotocol listeners shutting down too early)
         :param send_time_initiator: The time when the initiator submits a request for agreement.
         :param send_time_responder: The time when the responder submits a request for agreement.
         """
        initiator_node, responder_node = create_linear_network_with_routing(["Initiator", "Responder"],
                                                                            connection_delay=connection_delay)

        initiator_node.driver.add_service(
            EntanglementAgreementService,
            InitiativeAgreementService(
                node=initiator_node,
                delay_per_node={"Responder": connection_delay},
                mode=InitiativeBasedAgreementServiceMode.InitiativeTaking,
            ),
        )
        responder_node.driver.add_service(
            EntanglementAgreementService,
            InitiativeAgreementService(
                node=responder_node,
                delay_per_node={"Initiator": connection_delay},
                mode=InitiativeBasedAgreementServiceMode.Responding,
            ),
        )

        initiator_protocol = AgreementServiceTestProtocol(
            node=initiator_node,
            peer="Responder",
            requests=[ReqEntanglementAgreement("Responder")],
            send_times=[send_time_initiator],
            termination_delay=termination_delay,
        )
        responder_protocol = AgreementServiceTestProtocol(
            node=responder_node,
            peer="Initiator",
            requests=[ReqEntanglementAgreement("Initiator")],
            send_times=[send_time_responder],
            termination_delay=termination_delay,
        )

        initiator_node.driver.start_all_services()
        responder_node.driver.start_all_services()
        initiator_protocol.start()
        responder_protocol.start()

        ns.sim_run()

        self.check_agreement(initiator_protocol, responder_protocol, connection_delay)

    def check_agreement(self, initiator_protocol: AgreementServiceTestProtocol,
                        responder_protocol: AgreementServiceTestProtocol, connection_delay: float):
        submit_time_r = responder_protocol.submitted_agreement[0][0]
        submit_time_i = initiator_protocol.submitted_agreement[0][0]

        if submit_time_i + connection_delay < submit_time_r:
            rec_time_r = responder_protocol.received_agreement[0][0]
            rec_time_i = initiator_protocol.received_agreement[0][0]

            self.assertAlmostEqual(rec_time_i, rec_time_r)
            self.assertAlmostEqual(rec_time_i, submit_time_r + connection_delay)
        else:
            self.assertEqual(len(responder_protocol.received_agreement), 0)
            self.assertEqual(len(initiator_protocol.received_agreement), 0)
            self.assertEqual(len(responder_protocol.received_change), 1)

            abort_time_r = responder_protocol.received_reject[0][0]
            self.assertAlmostEqual(abort_time_r, submit_time_r)
            self.assertAlmostEqual(responder_protocol.received_change[-1], submit_time_i + connection_delay)

    def abort_test(
            self, connection_delay: float, termination_delay: float, send_time_initiator: float,
            send_time_responder: float, abort_offset, node_to_abort="Initiator",
    ):
        """
        Perform an abort test between nodes in a linear network.

        :param connection_delay: The classical communication delay between the nodes.
        :param termination_delay: The delay before terminating the agreement test.
        (prevents subprotocol listeners shutting down too early)
        :param send_time_initiator: The time when the initiator submits a request for agreement.
        :param send_time_responder: The time when the responder submits a request for agreement.
        :param abort_offset: The time when a node submits an abort request.
        :param node_to_abort: The node submitting the abort ("Initiator" or "Responder").
        """

        initiator_node, responder_node = create_linear_network_with_routing(["Initiator", "Responder"],
                                                                            connection_delay=connection_delay)

        initiator_node.driver.add_service(
            EntanglementAgreementService,
            InitiativeAgreementService(
                node=initiator_node,
                delay_per_node={"Responder": connection_delay},
                mode=InitiativeBasedAgreementServiceMode.InitiativeTaking,
            ),
        )
        responder_node.driver.add_service(
            EntanglementAgreementService,
            InitiativeAgreementService(
                node=responder_node,
                delay_per_node={"Initiator": connection_delay},
                mode=InitiativeBasedAgreementServiceMode.Responding,
            ),
        )

        initiator_protocol = AgreementServiceTestProtocol(
            node=initiator_node,
            peer="Responder",
            requests=[ReqEntanglementAgreement("Responder")],
            send_times=[send_time_initiator],
            termination_delay=termination_delay,
        )
        responder_protocol = AgreementServiceTestProtocol(
            node=responder_node,
            peer="Initiator",
            requests=[ReqEntanglementAgreement("Initiator")],
            send_times=[send_time_responder],
            termination_delay=termination_delay,
        )
        if node_to_abort == "Initiator":
            initiator_protocol.requests.append(ReqEntanglementAgreementAbort('Responder'))
            initiator_protocol.send_times.append(abort_offset)
        elif node_to_abort == "Responder":
            responder_protocol.requests.append(ReqEntanglementAgreementAbort('Initiator'))
            responder_protocol.send_times.append(abort_offset)
        else:
            raise KeyError("Incorrect node")

        initiator_node.driver.start_all_services()
        responder_node.driver.start_all_services()
        initiator_protocol.start()
        responder_protocol.start()

        ns.sim_run()

        self.check_abort(initiator_protocol, responder_protocol, connection_delay, node_to_abort)

    def check_abort(self, initiator_protocol: AgreementServiceTestProtocol,
                    responder_protocol: AgreementServiceTestProtocol, connection_delay: float, node_to_abort: str):
        submit_time_r = responder_protocol.submitted_agreement[-1][0]

        if node_to_abort == "Initiator":
            abort_time = initiator_protocol.submitted_abort[-1][0]
        else:
            abort_time = responder_protocol.submitted_abort[-1][0]

        if abort_time < submit_time_r + connection_delay:
            self._assert_no_agreement(initiator_protocol, responder_protocol)
        else:
            self.check_agreement(initiator_protocol, responder_protocol, connection_delay)

    def _assert_no_agreement(self, initiator_protocol: AgreementServiceTestProtocol,
                             responder_protocol: AgreementServiceTestProtocol):
        self.assertEqual(len(responder_protocol.received_agreement), 0)
        self.assertEqual(len(initiator_protocol.received_agreement), 0)

    def test_1(self):
        """Instant communication, responder submission after Initiator request arrived"""
        self.agreement_test(connection_delay=0, termination_delay=2,
                            send_time_responder=1, send_time_initiator=0)

    def test_2(self):
        """normal communication, responder submission after Initiator request arrived"""
        self.agreement_test(connection_delay=1, termination_delay=5,
                            send_time_responder=2, send_time_initiator=0)

    def test_3(self):
        """normal communication, responder submission before Initiator request arrived"""
        self.agreement_test(connection_delay=1, termination_delay=5,
                            send_time_responder=0, send_time_initiator=0)

    def test_4(self):
        """instant communication, Initiator abort arrives before responder submits"""
        self.abort_test(connection_delay=0, termination_delay=5,
                        send_time_responder=2, send_time_initiator=0, node_to_abort="Initiator",
                        abort_offset=1)

    def test_5(self):
        """instant communication, Responder abort before Initiator request arrived"""
        self.abort_test(connection_delay=0, termination_delay=5,
                        send_time_responder=0, send_time_initiator=2, node_to_abort="Responder",
                        abort_offset=1)

    def test_6(self):
        """normal communication, Initiator abort arrives before responder submits"""
        self.abort_test(connection_delay=1, termination_delay=8,
                        send_time_responder=3, send_time_initiator=0, node_to_abort="Initiator",
                        abort_offset=1)

    def test_7(self):
        """normal communication, Responder abort before Initiator request arrived"""
        self.abort_test(connection_delay=1, termination_delay=8,
                        send_time_responder=0, send_time_initiator=3, node_to_abort="Responder",
                        abort_offset=1)

    @unittest.expectedFailure  # responder will signal agreement, initiator does not
    def test_8(self):
        """normal communication, Initiator abort arrives at responder before confirmation arrives at initiator."""
        self.abort_test(connection_delay=2, termination_delay=10,
                        send_time_responder=3, send_time_initiator=0, node_to_abort="Initiator",
                        abort_offset=3)

    @unittest.expectedFailure  # initiator will signal agreement, responder does not
    def test_9(self):
        """normal communication, Responder aborts before confirmation arrives at initiator"""
        self.abort_test(connection_delay=2, termination_delay=10,
                        send_time_responder=3, send_time_initiator=0, node_to_abort="Responder",
                        abort_offset=4)

    def test_10(self):
        """normal communication, Initiator abort after agreement established. (Agreement is reached)"""
        self.abort_test(connection_delay=1, termination_delay=10,
                        send_time_responder=2, send_time_initiator=0, node_to_abort="Initiator",
                        abort_offset=5)

    def test_11(self):
        """normal communication, Responder abort after agreement established. (Agreement is reached)"""
        self.abort_test(connection_delay=1, termination_delay=10,
                        send_time_responder=2, send_time_initiator=0, node_to_abort="Responder",
                        abort_offset=5)


class AgreementServiceTestProtocol(NodeProtocol):
    """
    Protocol for testing entanglement agreement between nodes in a network.
    It registers agreement events, like submitting agreement requests or receiving agreement responses.

    :param node: The node to which the protocol is attached.
    :param peer: The name of the peer node.
    :param requests: List of entanglement agreement requests or abort requests to be sent.
    :param send_times: List of times at which to send the entanglement agreement requests.
    :param termination_delay: The delay before terminating the agreement test.
    (prevents subprotocol listeners shutting down too early)

    :ivar submitted_agreement: A list of tuples containing the timestamp and agreement requests submitted.
    :ivar submitted_abort: A list of tuples containing the timestamp and abort requests submitted.
    :ivar received_agreement: A list of tuples containing the timestamp and agreement responses received.
    :ivar received_reject: A list of tuples containing the timestamp and rejection responses received.
    :ivar received_change: A list of timestamps indicating changes in agreement status received.
    """
    def __init__(
        self,
        node: Node,
        peer: str,
        requests: List[Union[ReqEntanglementAgreement, ReqEntanglementAgreementAbort]],
        send_times: List[float],
        termination_delay: float,
    ):
        super().__init__(node=node)
        self.peer = peer
        self.requests = requests
        self.send_times = send_times
        self.listener_protocol = AgreementServiceTestProtocol.AgreementListener(self)
        self.termination_delay = termination_delay
        self.add_subprotocol(self.listener_protocol)
        self.submitted_agreement: List[(float, ReqEntanglementAgreement)] = []
        self.submitted_abort: List[(float, ReqEntanglementAgreementAbort)] = []
        self.received_agreement: List[(float, ResEntanglementAgreementReached)] = []
        self.received_reject: List[(float, ResEntanglementAgreementRejected)] = []
        self.received_change: List[float] = []

    def start(self):
        super().start()
        self.listener_protocol.start()

    def stop(self):
        super().stop()
        self.listener_protocol.stop()

    def run(self):
        agreement_service = self.node.driver[EntanglementAgreementService]

        assert len(self.requests) == len(self.send_times)
        for send_time, request in zip(self.send_times, self.requests):
            yield self.await_timer(end_time=send_time)
            agreement_service.put(request)

            if isinstance(request, ReqEntanglementAgreement):
                self.submitted_agreement.append((ns.sim_time(), request))
            elif isinstance(request, ReqEntanglementAgreementAbort):
                self.submitted_abort.append((ns.sim_time(), request))
            else:
                raise TypeError("Unexpected request type.")

        # Avoids early termination and early shutdown of AgreementListener
        yield self.await_timer(duration=self.termination_delay)

    class AgreementListener(NodeProtocol):
        def __init__(self, superprotocol: AgreementServiceTestProtocol):
            super().__init__(node=superprotocol.node)
            self.superprotocol = superprotocol

        def run(self):
            agreement_service = self.node.driver[EntanglementAgreementService]

            evt_rejected = self.await_signal(
                sender=agreement_service,
                signal_label=ResEntanglementAgreementRejected.__name__,
            )
            evt_agreement_reached = self.await_signal(
                sender=agreement_service,
                signal_label=ResEntanglementAgreementReached.__name__,
            )
            evt_agreement_change = self.await_signal(
                sender=agreement_service,
                signal_label=signal_entanglement_agreement_status_change,
            )
            while True:
                evt_expr = yield evt_agreement_reached | evt_rejected | evt_agreement_change
                [evt] = evt_expr.triggered_events
                res = agreement_service.get_signal_by_event(evt).result
                if isinstance(res, ResEntanglementAgreementRejected):
                    self.superprotocol.received_reject.append((ns.sim_time(), res))
                elif isinstance(res, ResEntanglementAgreementReached):
                    self.superprotocol.received_agreement.append((ns.sim_time(), res))
                else:
                    self.superprotocol.received_change.append(ns.sim_time())
