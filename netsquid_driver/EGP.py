from abc import ABCMeta, abstractmethod

from netsquid.protocols import ServiceProtocol
from qlink_interface import (
    ReqCreateAndKeep,
    ReqMeasureDirectly,
    ReqReceive,
    ReqRemoteStatePrep,
    ReqStopReceive,
    ResCreateAndKeep,
    ResError,
    ResMeasureDirectly,
    ResRemoteStatePrep,
)


class EGPService(ServiceProtocol, metaclass=ABCMeta):
    """EGP service. Defines interface for any EGP. EGP implementations should be subclassed from here.

    This class registers all requests and responses that EGP should use. The requests and responses themselves
    are available in the qlink-interface package. To create an EGP implementation, overwrite the abstract
    methods of EGP service and make sure the proper responses are sent.
    Additionally, EGPService generates create IDs which are returned when a request is made, and should also be
    included in the responses given by any EGP implementation.
    """

    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self.register_request(ReqCreateAndKeep, self.create_and_keep)
        self.register_request(ReqMeasureDirectly, self.measure_directly)
        self.register_request(ReqReceive, self.receive)
        self.register_request(ReqStopReceive, self.stop_receive)
        self.register_response(ResCreateAndKeep)
        self.register_response(ResMeasureDirectly)
        self.register_response(ResRemoteStatePrep)
        self.register_response(ResError)
        self._current_create_id = 0

    @abstractmethod
    def create_and_keep(self, req):
        """Distribute remote entanglement and store it at both parties.

        Parameters
        ----------
        req : :class:`qlink_interface.ReqCreateAndKeep`
            Request that needs to be handled by this method.

        Returns
        -------
        create_id : int
            Identifier that will also be used in responses corresponding to this request.

        Note
        ----

        Should call self.send_response(res).
        When successfull, res should be :class:`qlink_interface.ResCreateAndKeep`
        When unsuccessfull, res should be :class:`qlink_interface.ResError`.
        In both cases, the create_id of the response should correspond to the returned create_id.
        To generate a unique create_id, self._get_create_id can be used.

        """
        assert isinstance(req, ReqCreateAndKeep)

    @abstractmethod
    def measure_directly(self, req):
        """Distribute remote entanglement and measure as soon as possible at both parties.

        Parameters
        ----------
        req : :class:`qlink_interface.ReqMeasureDirectly`
            Request that needs to be handled by this method.

        Returns
        -------
        create_id : int
            Identifier that will also be used in responses corresponding to this request.

        Note
        ----

        Should call self.send_response(res).
        When successfull, res should be :class:`qlink_interface.ResMeasureDirectly`
        When unsuccessfull, res should be :class:`qlink_interface.ResError`.
        In both cases, the create_id of the response should correspond to the returned create_id.
        To generate a unique create_id, self._get_create_id can be used.

        """
        assert isinstance(req, ReqMeasureDirectly)

    @abstractmethod
    def remote_state_preparation(self, req):
        """Distribute remote entanglement and measure as soon as possible locally, but store it remotely.

        Parameters
        ----------
        req : :class:`qlink_interface.ReqRemoteStatePreparation`
            Request that needs to be handled by this method.

        Returns
        -------
        create_id : int
            Identifier that will also be used in responses corresponding to this request.

        Note
        ----

        Should call self.send_response(res).
        When successfull, res should be :class:`qlink_interface.ResRemoteStatePreparation`
        When unsuccessfull, res should be :class:`qlink_interface.ResError`.
        In both cases, the create_id of the response should correspond to the returned create_id.
        To generate a unique create_id, self._get_create_id can be used.

        """
        assert isinstance(req, ReqRemoteStatePrep)

    @abstractmethod
    def receive(self, req):
        """Allow entanglement generation with a remote EGP upon request by that EGP.

        Parameters
        ----------
        req : :class:`qlink_interface.ReqReceive`
            Request that needs to be handled by this method.

        """
        assert isinstance(req, ReqReceive)

    @abstractmethod
    def stop_receive(self, req):
        """Stop allowing entanglement generation with a remote EGP upon request by that EGP.

        Parameters
        ----------
        req : :class:`qlink_interface.ReqStopReceive`
            Request that needs to be handled by this method.

        """
        assert isinstance(req, ReqStopReceive)

    def _get_create_id(self):
        """Get a unique create ID to be used for identifying create requests.

        Returns
        -------
        create_id : int
            Unique identifier for create requests.

        """
        create_id = self._current_create_id
        self._current_create_id += 1
        return create_id
