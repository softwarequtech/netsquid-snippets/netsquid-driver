from abc import ABCMeta

import netsquid_driver.measurement_services as operation_services
from netsquid.components import INSTR_MEASURE, INSTR_MEASURE_BELL, INSTR_ROT, QuantumProgram
from netsquid.protocols import NodeProtocol
from netsquid.qubits.ketstates import BellIndex
from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService, ResLocalDiscard


class ProcessingNodeMeasureProgram(QuantumProgram):
    """Implements arbitrary-basis single-qubit measurement circuit on a quantum processor.

    The measurement is done in any desired basis by first performing a rotation according to the
    specified Euler angles.
    The operations that need to be supported by the quantum processor are
    `netsquid.components.instructions.INSTR_ROT` and `netsquid.components.instructions.INSTR_MEASURE`.

    """

    def program(self, x_rotation_angle_1, y_rotation_angle, x_rotation_angle_2):
        qubit_to_meas = self.get_qubit_indices(1)
        self.apply(instruction=INSTR_ROT, qubit_indices=qubit_to_meas, angle=x_rotation_angle_1, axis=(1, 0, 0))
        self.apply(instruction=INSTR_ROT, qubit_indices=qubit_to_meas, angle=y_rotation_angle, axis=(0, 1, 0))
        self.apply(instruction=INSTR_ROT, qubit_indices=qubit_to_meas, angle=x_rotation_angle_2, axis=(1, 0, 0))
        self.apply(instruction=INSTR_MEASURE, qubit_indices=qubit_to_meas, output_key="outcome")
        yield self.run()


class ProcessingNodeSwapProgram(QuantumProgram):
    """Implements Bell-state measurement on a quantum processor.

    The only operation that needs to be supported by the quantum processor is
    `netsquid.components.instructions.INSTR_MEASURE_BELL`.

    """

    def program(self):
        q1, q2 = self.get_qubit_indices(2)
        self.apply(INSTR_MEASURE_BELL, [q1, q2], output_key="bsm_outcome", inplace=False)
        yield self.run()

    @property
    def outcome_as_netsquid_bell_index(self):
        return BellIndex(self.output.get("bsm_outcome")[0])


class ProcessingNodeMeasureService(operation_services.MeasureService):
    """Service for performing a single-qubit measurement on a processing node.

    By default uses :class:`ProcessingNodeMeasureProgram` to perform the measurement, which works on any
    :class:`netsquid.components.qprocessor.QuantumProcessor` that supports
    `netsquid.components.instructions.INSTR_ROT` and `netsquid.components.instructions.INSTR_MEASURE`.
    For processors that do not support these operations, another program can be set using the `meas_prog` argument.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the measurement is registered at the entanglement tracker.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'ProcessingNodeMeasureService'.
    meas_prog : :class:`netsquid.components.qprogram.QuantumProgram`
        Quantum program used to perform measurement.
        Should take arguments `x_rotation_angle_1`, `y_rotation_angle` and `x_rotation_angle_2`.
        Defaults to :class:`ProcessingNodeMeasureProgram`.

    """

    def __init__(self, node, name="ProcessingNodeMeasureService", meas_prog=ProcessingNodeMeasureProgram):
        super().__init__(node=node, name=name)

        self._meas_prog = meas_prog()
        self._mem_pos = None
        self._x_rotation_angle_1 = None
        self._y_rotation_angle = None
        self._x_rotation_angle_2 = None
        self._measure_signal = "measure_signal"
        self.add_signal(self._measure_signal)
        self._entanglement_tracker = None

    def run(self):
        try:
            self._entanglement_tracker = self.node.driver[EntanglementTrackerService]
        except (AttributeError, KeyError):
            pass
        while True:
            yield self.await_signal(sender=self, signal_label=self._measure_signal)
            yield from self._perform_measurement()

    def _perform_measurement(self):
        if None in [self._mem_pos, self._x_rotation_angle_1, self._y_rotation_angle, self._x_rotation_angle_2]:
            raise Exception("At least one of the required arguments was not specified.")
        self.node.qmemory.execute_program(self._meas_prog,
                                          qubit_mapping=[self._mem_pos],
                                          x_rotation_angle_1=self._x_rotation_angle_1,
                                          y_rotation_angle=self._y_rotation_angle,
                                          x_rotation_angle_2=self._x_rotation_angle_2)
        yield self.await_program(self.node.qmemory)
        if self._entanglement_tracker is not None:
            self._entanglement_tracker.register_local_measurement_mem_pos(mem_pos=self._mem_pos)
        self.send_response(operation_services.ResMeasure(outcome=self._meas_prog.output["outcome"]))

    def measure(self, req):
        self._mem_pos = req.mem_pos
        self._x_rotation_angle_1 = req.x_rotation_angle_1
        self._y_rotation_angle = req.y_rotation_angle
        self._x_rotation_angle_2 = req.x_rotation_angle_2

        self.send_signal(self._measure_signal)


class ProcessingNodeSwapService(operation_services.SwapService):
    """Service for entanglement swapping (i.e. performing a Bell-state measurement) on a processing node.

    By default uses :class:`AbstractSwapProgramWithBellIndex` to perform the measurement, which works on any
    :class:`netsquid.components.qprocessor.QuantumProcessor` that supports
    `netsquid.components.instructions.INSTR_CNOT`, `netsquid.components.instructions.INSTR_H`
    and `netsquid.components.instructions.INSTR_MEASURE`.
    For processors that do not support these operations, another program can be set using the `meas_prog` argument.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the swap is registered at the entanglement tracker.

    If a discard is registered at the local `EntanglementTrackerService` while the swap program is being executed,
    the swap is considered to have failed, and all participating qubits are discarded (if they weren't already).

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'ProcessingNodeSwapService'.
    swap_prog : :class:`netsquid.components.qprogram.QuantumProgram`
        Quantum program used to perform entanglement swap.
        Defaults to :class:`AbstractSwapProgramWithBellIndex`.

    """

    class _ListenForDiscardsSubprotocol(NodeProtocol):
        """Protocol that listens to entanglement tracker to find out if one of the qubits is discarded during swap."""

        def __init__(self, node):
            super().__init__(node=node, name=None)
            self.there_has_been_a_discard = False
            self._entanglement_tracker = None
            self._mem_pos = []

        def set_memory_positions(self, memory_positions):
            self._mem_pos = memory_positions

        def run(self):
            self.there_has_been_a_discard = False
            try:
                self._entanglement_tracker = self.node.driver[EntanglementTrackerService]
            except (AttributeError, KeyError):
                return
            while True:
                yield self.await_signal(sender=self._entanglement_tracker, signal_label=ResLocalDiscard.__name__)
                response = self._entanglement_tracker.get_signal_result(ResLocalDiscard.__name__)
                if response.memory_position in self._mem_pos:
                    self.there_has_been_a_discard = True
                    break

    def __init__(self, node, name="ProcessingNodeSwapService", swap_prog=ProcessingNodeSwapProgram):
        super().__init__(node=node, name=name)
        self._mem_pos_1 = None
        self._mem_pos_2 = None
        self._swap_signal = "swap_signal"
        self.add_signal(self._swap_signal)
        self._swap_prog = swap_prog()
        self._entanglement_tracker = None
        self._cutoff_service = None
        self.add_subprotocol(name="listen_for_discard", subprotocol=self._ListenForDiscardsSubprotocol(node=self.node))

    def run(self):
        try:
            self._entanglement_tracker = self.node.driver[EntanglementTrackerService]
        except (AttributeError, KeyError):
            pass
        while True:
            yield self.await_signal(sender=self, signal_label=self._swap_signal)
            yield from self._perform_swap()

    def _perform_swap(self):
        if self._mem_pos_1 == self._mem_pos_2:
            raise ValueError("Needs two qubits to perform a Bell-state measurement")
        if None in [self._mem_pos_1, self._mem_pos_2]:
            raise Exception("At least one of the required arguments was not specified.")
        self.node.qmemory.execute_program(self._swap_prog, qubit_mapping=[self._mem_pos_1, self._mem_pos_2])
        if self.node.qmemory.busy:
            # only need to await the program and listen for discards if the duration of the program is not 0
            self._start_listening_for_discards()
            yield self.await_program(self.node.qmemory)

        if self._success:
            bell_index = self._swap_prog.outcome_as_netsquid_bell_index
            self._register_swap_with_entanglement_tracker(bell_index)
        else:
            bell_index = None
            self._register_failed_swap_with_entanglement_tracker()
        self.send_response(operation_services.ResSwap(outcome=bell_index))

    @property
    def _success(self):
        """Whether the swap has been successful or not."""
        success = True
        if self._there_has_been_a_discard:
            success = False
        return success

    def _start_listening_for_discards(self):
        self.subprotocols["listen_for_discard"].reset()
        self.subprotocols["listen_for_discard"].set_memory_positions([self._mem_pos_1, self._mem_pos_2])

    @property
    def _there_has_been_a_discard(self):
        return self.subprotocols["listen_for_discard"].there_has_been_a_discard

    def _register_swap_with_entanglement_tracker(self, bell_index):
        if self._entanglement_tracker is not None:
            self._entanglement_tracker. \
                register_local_swap_mem_pos(mem_pos_1=self._mem_pos_1,
                                            mem_pos_2=self._mem_pos_2,
                                            bell_index=bell_index)

    def _register_failed_swap_with_entanglement_tracker(self):
        if self._entanglement_tracker is not None:
            self._entanglement_tracker.register_local_discard_mem_pos(self._mem_pos_1)
            self._entanglement_tracker.register_local_discard_mem_pos(self._mem_pos_2)

    def swap(self, req):
        super().swap(req=req)
        self._mem_pos_1 = req.mem_pos_1
        self._mem_pos_2 = req.mem_pos_2

        self.send_signal(self._swap_signal)


class IBellStateMeasurementProgram(QuantumProgram, metaclass=ABCMeta):
    """
    Attributes
    -----------
    outcome : int
           The outcome of the Bell-state-measurement using
           the following translaten: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-.

    Internal working
    ----------------
    A few private attributes:
      * _NAME_OUTCOME_CONTROL : str
      * _NAME_OUTCOME_TARGET : str
      * _OUTCOME_TO_BELL_INDEX : dict with keys (int, int) and values int
           Indicates how the two measurement outcomes are related to the
           state that is measured. Its keys are tuples of the two measurement
           outcomes (control, target) and its values is the Bell state index
           using the following translaten: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-.
    """

    default_num_qubits = 2
    _NAME_OUTCOME_CONTROL = "control-qubit-outcome"
    _NAME_OUTCOME_TARGET = "target-qubit-outcome"
    _OUTCOME_TO_BELL_INDEX = {(x, y): None for x in [0, 1] for y in [0, 1]}

    @property
    def get_outcome_as_bell_index(self):
        m_outcome_control = self.output[self._NAME_OUTCOME_CONTROL][0]
        m_outcome_target = self.output[self._NAME_OUTCOME_TARGET][0]
        return self._OUTCOME_TO_BELL_INDEX[(m_outcome_control, m_outcome_target)]
