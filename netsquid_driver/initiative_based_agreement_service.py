from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from typing import Dict, List, Tuple

from netsquid.protocols import NodeProtocol
from netsquid_driver.classical_routing_service import ClassicalRoutingService, RemoteServiceRequest
from netsquid_driver.entanglement_agreement_service import (
    EntanglementAgreementService,
    ReqEntanglementAgreement,
    ReqEntanglementAgreementAbort,
    ResEntanglementAgreementReached,
    ResEntanglementAgreementRejected,
)


@dataclass
class ReqBaseAgreementInteractionMessage:
    origin_node_name: str
    remote_node_name: str
    connection_id: int
    _msg = ""

    def __str__(self):
        return f"{self.__class__.__name__}. origin_node_name:{self.origin_node_name}, " \
               f"remote_node_name:{self.remote_node_name}, " \
               f"connection_id:{self.connection_id}, " \
               f"{self._msg}"


@dataclass
class ReqAskAgreement(ReqBaseAgreementInteractionMessage):
    _msg = "We are going to generate entanglement, or else..."


@dataclass
class ReqConfirmAgreement(ReqBaseAgreementInteractionMessage):
    _msg = "Yes I will generate entanglement with you, please don't hurt my kids!"


@dataclass
class ReqRetractAskAgreement(ReqBaseAgreementInteractionMessage):
    _msg = "Message: Keep your filthy entanglement to yourself!"


class InitiativeBasedAgreementServiceMode(Enum):
    InitiativeTaking = 1
    Responding = 2


class InitiativeAgreementService(EntanglementAgreementService):
    """Reach agreement about entanglement generation between initiative-taking nodes and responding nodes.

    This implementation of :class:`EntanglementAgreementService` is designed to reach agreement about
    entanglement generation with a remote node running a different mode.

    If the mode is `InitiativeBasedAgreementServiceMode.InitiativeTaking`, it tries to reach agreement
    about entanglement generation by asking for it and waiting for confirmation.
    It then sends messages to remote nodes asking for agreement when a :class:`ReqEntanglementAgreement` is issued.
    :class:`ResEntanglementAgreementReached` response is sent if a confirmation message is received
    from a remote node that has been asked for agreement.

    If the mode is `InitiativeBasedAgreementServiceMode.Responding` this service tries to reach agreement
    about entanglement generation by responding to other nodes taking the initiative.
    It then only handles a :class:`ReqEntanglementAgreement` if it has previously received a message asking for
    agreement, and will respond by sending a confirmation message back and giving a
    :class:`ResEntanglementAgreementReached` response after some set delay.

    If a request indicating that entanglement with a remote node is not desired anymore is received,
    this service in Responding mode will start rejecting requests with that remote node over that
    connection again and if in InitiativeTaking mode it will send a message retracting agreement to the remote node and
    ignore any confirmation it receives from that node.
    In both modes, this request will prevent a local response :class:`ResEntanglementAgreementReached`,
    but may be too late to prevent a response on the remote node.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    delay_per_node : dict
        Dictionary mapping node names to the delay for classical communication to the remote node.
    mode : :class:`InitiativeBasedAgreementServiceMode`
        The mode of the agreement service.
    name : str, optional
        The name of the protocol.
    """

    def __init__(self, node, delay_per_node: Dict[str, float],
                 mode: InitiativeBasedAgreementServiceMode, name=None):
        super().__init__(node=node, name=name)
        self.register_request(ReqAskAgreement, self._handle_ask_agreement)
        self.register_request(ReqConfirmAgreement, self._handle_confirm_agreement)
        self.register_request(ReqRetractAskAgreement, self._handle_retract_ask_agreement)
        self._nodes_and_connections_with_outstanding_agreement_requests: List[Tuple[str, int]] = []
        self._nodes_and_connections_that_want_agreement = []
        self._nodes_and_connections_with_which_agreement_is_being_confirmed = []
        self._send_response_after_delay_protocols = []
        self._delay_per_node = delay_per_node
        self._mode = mode

    @property
    def routing_service(self) -> ClassicalRoutingService:
        return self.node.driver.services[ClassicalRoutingService]

    def is_connected(self):
        return ClassicalRoutingService in self.node.driver.services.keys()

    def _send_message(self, req: ReqBaseAgreementInteractionMessage):
        service_req = RemoteServiceRequest(request=req, service=EntanglementAgreementService,
                                           targets=[req.remote_node_name], origin=req.origin_node_name)
        self.routing_service.put(service_req)

    def try_to_reach_agreement(self, req: ReqEntanglementAgreement):
        if self._mode is InitiativeBasedAgreementServiceMode.InitiativeTaking:
            self._try_to_reach_agreement_initiative(req)
        else:
            self._try_to_reach_agreement_responding(req)

    def abort(self, req: ReqEntanglementAgreementAbort):
        if self._mode is InitiativeBasedAgreementServiceMode.InitiativeTaking:
            self._abort_initiative(req)
        else:
            self._abort_responding(req)

    def _abort_initiative(self, req: ReqEntanglementAgreementAbort):
        """Send message to retract asking for agreement, and stop listening for confirmation messages.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreementAbort`
            Request specifying which attempt at agreement should be aborted.

        """
        self._send_message(ReqRetractAskAgreement(self.node.name, req.remote_node_name, req.connection_id))
        if (req.remote_node_name, req.connection_id) in self._nodes_and_connections_with_outstanding_agreement_requests:
            self._nodes_and_connections_with_outstanding_agreement_requests.remove(
                (req.remote_node_name, req.connection_id))

    def _abort_responding(self, req):
        """If currently waiting for a delayed `ResEntanglementAgreementReached`, stop waiting and don't send response.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreementAbort`
            Request specifying the obtaining of which agreement should be aborted.

        """
        for protocol in self._send_response_after_delay_protocols:
            if protocol.remote_node_name == req.remote_node_name:
                protocol.stop()

    def _try_to_reach_agreement_initiative(self, req: ReqEntanglementAgreement):
        """Send a message asking for agreement and wait for a confirmation.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreement`
            Request holding requirements on the agreement that must be reached.

        """
        if (req.remote_node_name, req.connection_id) in self._nodes_and_connections_with_outstanding_agreement_requests:
            raise RuntimeError(f"Already trying to get agreement with node {req.remote_node_name} through connection "
                               f"{req.connection_id}, cannot handle two of the same requests at once.")

        self._send_message(ReqAskAgreement(self.node.name, req.remote_node_name, req.connection_id))
        self._nodes_and_connections_with_outstanding_agreement_requests.append(
            (req.remote_node_name, req.connection_id))

    def _try_to_reach_agreement_responding(self, req: ReqEntanglementAgreement):
        """Reject request for agreement if not asked for agreement by other node, otherwise confirm to that node.

        If no message asking this node for agreement has been received from the remote node specified in the request,
        it is immediately rejected.
        Otherwise, a confirmation message is sent to the remote node indicating that this node wants to reach agreement.
        This is considered enough to actually reach agreement with the remote node;
        after a time set by :meth:`RespondingAgreementService.determine_delay`, a
        :class:`ResEntanglementAgreementReached` will be sent.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreement`
            Request holding requirements on the agreement that must be reached.

        """
        if (req.remote_node_name, req.connection_id) in \
                self._nodes_and_connections_with_which_agreement_is_being_confirmed:
            raise RuntimeError(f"Already confirming agreement with with node {req.remote_node_name} through connection "
                               f"{req.connection_id}. Cannot get agreement with the same node through the same "
                               f"connection twice simultaneously.")
        elif (req.remote_node_name, req.connection_id) not in self._nodes_and_connections_that_want_agreement:
            self.send_response(ResEntanglementAgreementRejected(remote_node_name=req.remote_node_name,
                                                                connection_id=req.connection_id))
        else:
            self._nodes_and_connections_with_which_agreement_is_being_confirmed.append((req.remote_node_name,
                                                                                        req.connection_id))
            self._send_message(ReqConfirmAgreement(self.node.name, req.remote_node_name, req.connection_id))
            send_response_after_delay_protocol = \
                self._SendResponseAfterDelay(superprotocol=self,
                                             delay=self._delay_per_node[req.remote_node_name],
                                             remote_node_name=req.remote_node_name, connection_id=req.connection_id)
            self._send_response_after_delay_protocols.append(send_response_after_delay_protocol)
            self._nodes_and_connections_that_want_agreement.remove((req.remote_node_name, req.connection_id))
            send_response_after_delay_protocol.start()

    class _SendResponseAfterDelay(NodeProtocol):
        """Protocol used by `InitiativeAgreementService` in responding mode to send
        `ResEntanglementAgreementReached` with a delay.

        Parameters
        ----------
        superprotocol : :class:`RespondingAgreementService`
            Protocol that is using this protocol to send a delayed response.
        delay : float
            Amount of time [ns] the response should be delayed.
        remote_node_name : str
            name of the remote node with which entanglement agreement should be claimed to have been reached
            after the delay.
        connection_id : int
            ID of the entangling connection about which agreement should be reached.

        """
        def __init__(self, superprotocol: InitiativeAgreementService, delay: float,
                     remote_node_name: str, connection_id: int):
            super().__init__(node=superprotocol.node, name="send_response_after_delay")
            self.superprotocol = superprotocol
            self.delay = delay
            self.remote_node_name = remote_node_name
            self.connection_id = connection_id

        def run(self):
            if self.delay > 0:
                yield self.await_timer(duration=self.delay)
            self.superprotocol.send_response(ResEntanglementAgreementReached(remote_node_name=self.remote_node_name,
                                                                             connection_id=self.connection_id))

        def stop(self):
            super().stop()
            self.superprotocol._nodes_and_connections_with_which_agreement_is_being_confirmed.\
                remove((self.remote_node_name, self.connection_id))
            self.superprotocol._send_response_after_delay_protocols.remove(self)

    def _handle_confirm_agreement(self, req: ReqConfirmAgreement):
        """Handle confirmation messages.

        Parameters
        ----------
        req : :class:`initiative_based_agreement_service.ReqConfirmAgreement
            Received message.

        Notes
        -----
        It is tempting to raise an error when receiving a confirmation message from a node with which there is no
        outstanding agreement request, as in that case the service does not know how to deal with the message.
        However, it may be that agreement was requested with a node, but then aborted afterwards at this node.
        The other node might then still send a confirmation message, as it doesn't know about the abortion.
        Since this is somewhat common, it is better to ignore these confirmation messages.
        """
        remote_node_name = req.origin_node_name
        connection_id = req.connection_id
        if self._mode is InitiativeBasedAgreementServiceMode.Responding:
            raise RuntimeError(f"Initiative based agreement service in responding mode"
                               f" received a confirm agreement request: {req}")

        if (remote_node_name, connection_id) in \
                self._nodes_and_connections_with_outstanding_agreement_requests:
            self.send_response(ResEntanglementAgreementReached(remote_node_name=remote_node_name,
                                                               connection_id=connection_id))
            self._nodes_and_connections_with_outstanding_agreement_requests.remove((remote_node_name,
                                                                                    connection_id))
        else:
            # We do not raise an error in this case, because the receiving of an unexpected confirmation
            # message can be the result of a previously issued abort request.
            # See also the Notes section of the docstring of this method.
            # raise RuntimeError("Received confirmation message of node that has not been asked for agreement.")
            pass

    def _handle_ask_agreement(self, req: ReqAskAgreement):
        if self._mode is InitiativeBasedAgreementServiceMode.InitiativeTaking:
            raise RuntimeError(f"Initiative based agreement service in InitiativeTaking mode"
                               f" received a ask agreement request: {req}")

        remote_node_name = req.origin_node_name
        connection_id = req.connection_id
        if (remote_node_name, connection_id) in \
                self._nodes_and_connections_that_want_agreement:
            raise RuntimeError("A node-connection pair has asked for agreement twice.")
        self._nodes_and_connections_that_want_agreement.append((remote_node_name, connection_id))
        self._send_update_signal()

    def _handle_retract_ask_agreement(self, req: ReqRetractAskAgreement):
        if self._mode is InitiativeBasedAgreementServiceMode.InitiativeTaking:
            raise RuntimeError(f"Initiative based agreement service in InitiativeTaking mode"
                               f" received a retract ask agreement request: {req}")
        remote_node_name = req.origin_node_name
        connection_id = req.connection_id
        if (remote_node_name, connection_id) in self._nodes_and_connections_that_want_agreement:
            self._nodes_and_connections_that_want_agreement.remove((remote_node_name, connection_id))
        elif (remote_node_name, connection_id) in \
                self._nodes_and_connections_with_which_agreement_is_being_confirmed:
            self.abort(req=ReqEntanglementAgreementAbort(remote_node_name=remote_node_name,
                                                         connection_id=connection_id))
        else:
            # We do not raise an error in this case, since such an "unexpected" message can occur when
            # the other node aborts generating agreement while the confirmation message is already underway.
            # See also the Notes section of the docstring of this method.
            # raise RuntimeError("A node that has not asked for agreement is now retracting it.")
            pass
