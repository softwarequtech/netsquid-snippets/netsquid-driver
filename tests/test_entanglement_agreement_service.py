from dataclasses import dataclass

import netsquid as ns
from netsquid.nodes.node import Node
from netsquid.protocols import NodeProtocol
from netsquid_driver.classical_routing_service import ClassicalRoutingService, RemoteServiceRequest
from netsquid_driver.entanglement_agreement_service import (
    EntanglementAgreementService,
    ReqEntanglementAgreement,
    ReqEntanglementAgreementAbort,
    ResEntanglementAgreementReached,
    ResEntanglementAgreementRejected,
    signal_entanglement_agreement_status_change,
)
from util import create_linear_network_with_routing


class MockEntanglementAgreementService(EntanglementAgreementService):
    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self.aborted_node_names = []

    def try_to_reach_agreement(self, req: ReqEntanglementAgreement):
        if req.remote_node_name == "Node0":
            response = ResEntanglementAgreementRejected(remote_node_name=req.remote_node_name, connection_id=0)
        elif req.remote_node_name == "Node1":
            response = ResEntanglementAgreementReached(remote_node_name=req.remote_node_name, connection_id=0)
        else:
            raise ValueError
        self.send_response(response)

    def abort(self, req: ReqEntanglementAgreementAbort):
        self.aborted_node_names.append(req.remote_node_name)


class EntanglementAgreementServiceTestProtocol(NodeProtocol):

    def __init__(self, node, mock_entanglement_agreement_service):
        super().__init__(node=node, name="entanglement_agreement_service_test_protocol")
        self.agreement_service = mock_entanglement_agreement_service
        self.checked_rejection = False
        self.checked_agreement_reached = False
        self.checked_update_signal = False
        self.checked_abort = False

    def check_rejection(self):
        req = ReqEntanglementAgreement(remote_node_name="Node0", connection_id=0)
        self.agreement_service.put(req)
        yield self.await_signal(sender=self.agreement_service, signal_label=ResEntanglementAgreementRejected.__name__)
        self.checked_rejection = True

    def check_agreement_reached(self):
        req = ReqEntanglementAgreement(remote_node_name="Node1", connection_id=0)
        self.agreement_service.put(req)
        yield self.await_signal(sender=self.agreement_service, signal_label=ResEntanglementAgreementReached.__name__)
        self.checked_agreement_reached = True

    def check_update_signal(self):
        self.agreement_service._send_update_signal()
        yield self.await_signal(sender=self.agreement_service, signal_label=signal_entanglement_agreement_status_change)
        self.checked_update_signal = True

    def check_abort(self):
        req = ReqEntanglementAgreementAbort(remote_node_name="Node10", connection_id=0)
        self.agreement_service.put(req)
        assert self.agreement_service.aborted_node_names == ["Node10"]
        self.checked_abort = True

    def run(self):
        yield from self.check_rejection()
        yield from self.check_agreement_reached()
        yield from self.check_update_signal()
        self.check_abort()


def test_entanglement_agreement_service():
    ns.sim_reset()
    node = Node(name="Node")
    mock_agreement_service = MockEntanglementAgreementService(node=node)
    agreement_test_protocol = \
        EntanglementAgreementServiceTestProtocol(node=node,
                                                 mock_entanglement_agreement_service=mock_agreement_service)
    agreement_test_protocol.start()
    ns.sim_run()
    assert agreement_test_protocol.checked_rejection
    assert agreement_test_protocol.checked_agreement_reached
    assert agreement_test_protocol.checked_update_signal
    assert agreement_test_protocol.checked_abort


@dataclass
class ReqMockAskAgreement:
    origin_node_name: str
    connection_id: int


@dataclass
class ReqMockConfirmAgreement:
    origin_node_name: str
    connection_id: int


class MockSenderEntanglementAgreementService(EntanglementAgreementService):
    """Send aks request to reach agreement. Agreement reached if a confirm is sent back."""
    def __init__(self, node):
        super().__init__(node=node, name=None)
        self.remote_node_name = None
        self.finished = False
        self.register_request(ReqMockConfirmAgreement, self._handle_confirm)

    def try_to_reach_agreement(self, req: ReqEntanglementAgreement):
        self.remote_node_name = req.remote_node_name
        self._send_ask(req)

    def _handle_confirm(self, req: ReqMockConfirmAgreement):
        assert req.origin_node_name == self.remote_node_name
        self.finished = True

    def _send_ask(self, req: ReqEntanglementAgreement):
        req_ask = ReqMockAskAgreement(origin_node_name=self.node.name, connection_id=req.connection_id)
        remote_service_req = RemoteServiceRequest(request=req_ask,
                                                  service=EntanglementAgreementService,
                                                  origin=self.node.name,
                                                  targets=[req.remote_node_name])
        self.node.driver.services[ClassicalRoutingService].put(remote_service_req)

    def is_connected(self):
        return ClassicalRoutingService in self.node.driver.services

    def abort(self, req):
        raise NotImplementedError


class MockEchoEntanglementAgreementService(EntanglementAgreementService):
    """Echo back an ask entanglement agreement to the same node, but as a confirm agreement request. """
    def __init__(self, node):
        super().__init__(node=node, name=None)
        self.register_request(ReqMockAskAgreement, self._handle_ask)

    def try_to_reach_agreement(self, req: ReqEntanglementAgreement):
        raise NotImplementedError

    def _handle_ask(self, req: ReqMockAskAgreement):
        self._send_confirm(req)

    def _send_confirm(self, req: ReqMockAskAgreement):
        req_ask = ReqMockConfirmAgreement(origin_node_name=self.node.name, connection_id=req.connection_id)
        remote_service_req = RemoteServiceRequest(request=req_ask,
                                                  service=EntanglementAgreementService,
                                                  origin=self.node.name,
                                                  targets=[req.origin_node_name])
        self.node.driver.services[ClassicalRoutingService].put(remote_service_req)

    def is_connected(self):
        return ClassicalRoutingService in self.node.driver.services

    def abort(self, req):
        raise NotImplementedError


def mock_entanglement_agreement_service_with_classical_connections_test(delay, expected_duration):
    ns.sim_reset()

    node_sender, node_receiver = create_linear_network_with_routing(["Sender", "Receiver"], connection_delay=delay)

    sender_agreement_service = MockSenderEntanglementAgreementService(node=node_sender)
    node_sender.driver.add_service(EntanglementAgreementService, sender_agreement_service)

    receiver_agreement_service = MockEchoEntanglementAgreementService(node=node_receiver)
    node_receiver.driver.add_service(EntanglementAgreementService, receiver_agreement_service)

    node_receiver.driver.start_all_services()
    node_sender.driver.start_all_services()

    req = ReqEntanglementAgreement(remote_node_name=node_receiver.name, connection_id=0)
    sender_agreement_service.put(req)
    ns.sim_run()
    assert ns.sim_time() == expected_duration
    assert sender_agreement_service.finished


def test_mock_entanglement_agreement_service_with_classical_connection(delay=50):
    expected_duration = 2 * delay  # round trip time of connection
    mock_entanglement_agreement_service_with_classical_connections_test(delay=delay,
                                                                        expected_duration=expected_duration)
