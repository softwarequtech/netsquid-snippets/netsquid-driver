import netsquid as ns
from netsquid.nodes.node import Node
from netsquid.protocols.nodeprotocols import NodeProtocol
from netsquid.qubits.ketstates import BellIndex
from netsquid_driver.entanglement_agreement_service import (
    EntanglementAgreementService,
    ResEntanglementAgreementReached,
    ResEntanglementAgreementRejected,
)
from netsquid_driver.entanglement_service import (
    EntanglementService,
    ReqEntanglement,
    ReqEntanglementAbort,
    ResEntanglementError,
    ResEntanglementFail,
    ResEntanglementSuccess,
    ResStateReady,
)


class SimpleEntanglement(EntanglementService):
    """Implementation of :class:`entanglement_service.EntanglementService` meant to test request and response
    handling."""

    def __init__(self, node):
        super().__init__(node=node, name="simple_entanglement_service")
        self.performed_generate_entanglement = False
        self.performed_abort = False

    def generate_entanglement(self, req):
        super().generate_entanglement(req)
        self.performed_generate_entanglement = True

    def abort(self, req):
        super().abort(req)
        self.performed_abort = True


class EntanglementAgreementOnlyWithSomeNodes(EntanglementAgreementService):
    def __init__(self, node):
        super().__init__(node=node, name=None)
        self._allowed_node_ids = []
        self._send_response_to_these_node_ids = []
        self.aborted = []

    def add_allowed_node(self, node_id, send_response=True):
        """If not send_response, requests with this node will not be rejected, but also not accepted."""
        self._allowed_node_ids.append(node_id)
        if send_response:
            self._send_response_to_these_node_ids.append(node_id)
        self._send_update_signal()

    def remove_allowed_node(self, node_id):
        self._allowed_node_ids.remove(node_id)
        self._send_response_to_these_node_ids.remove(node_id)

    def reset_allowed_nodes(self):
        self._allowed_node_ids = []
        self._send_response_to_these_node_ids = []

    def try_to_reach_agreement(self, req):
        super().try_to_reach_agreement(req)
        if req.remote_node_name in self._allowed_node_ids:
            if req.remote_node_name in self._send_response_to_these_node_ids:
                response = ResEntanglementAgreementReached(remote_node_name=req.remote_node_name,
                                                           connection_id=req.connection_id)
                self.send_response(response)
        else:
            response = ResEntanglementAgreementRejected(remote_node_name=req.remote_node_name,
                                                        connection_id=req.connection_id)
            self.send_response(response)

    def abort(self, req):
        self.aborted.append(req.remote_node_name)


class EntanglementServiceTestProtocol(NodeProtocol):
    """Protocol to test responses from a :class:`entanglement_service.EntanglementService`.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node` or None, optional
        Node this protocol runs on. If None, a node should be set later before starting
        this protocol.
    entanglement_service : :class:`entanglement_service.EntanglementService`.
        Service to be tested for responses.
    name : str or None, optional
        Name of protocol. If None, the name of the class is used.

    """
    def __init__(self, node, entanglement_service, name="entanglement_service_test_protocol"):
        super().__init__(node=node, name=name)
        self.entanglement_service = entanglement_service
        self.finished = False

    def run(self):

        signal_res_succ = ResEntanglementSuccess.__name__
        signal_res_fail = ResEntanglementFail.__name__
        signal_res_state = ResStateReady.__name__
        signal_res_error = ResEntanglementError.__name__

        yield self.await_signal(sender=self.entanglement_service, signal_label=signal_res_succ)
        res_succ = self.entanglement_service.get_signal_result(signal_res_succ)
        assert isinstance(res_succ, ResEntanglementSuccess)
        BellIndex(res_succ.bell_index)

        yield self.await_signal(sender=self.entanglement_service, signal_label=signal_res_fail)
        res_fail = self.entanglement_service.get_signal_result(signal_res_fail)
        assert isinstance(res_fail, ResEntanglementFail)

        yield self.await_signal(sender=self.entanglement_service, signal_label=signal_res_state)
        res_state = self.entanglement_service.get_signal_result(signal_res_state)
        assert isinstance(res_state, ResStateReady)

        yield self.await_signal(sender=self.entanglement_service, signal_label=signal_res_error)
        res_error = self.entanglement_service.get_signal_result(signal_res_error)
        assert isinstance(res_error, ResEntanglementError)
        assert res_error.error_code == 3

        self.finished = True


def test_entanglement_service():
    """Test if requests and responses are handled correctly by :class:`entanglement_service.EntanglementService`."""

    node = Node("test_entanglement_service_node")
    entanglement_service = SimpleEntanglement(node=node)
    test_protocol = EntanglementServiceTestProtocol(node=node, entanglement_service=entanglement_service)

    # First test if entanglement requests are handled.
    req_ent = ReqEntanglement(remote_node_name="remote_node", mem_pos=0)
    entanglement_service.put(req_ent)
    assert entanglement_service.performed_generate_entanglement
    assert not entanglement_service.performed_abort

    # Now test if abortion requests are handled.
    req_abort = ReqEntanglementAbort(remote_node_name="remote_node")
    entanglement_service.put(req_abort)
    assert entanglement_service.performed_abort

    ns.sim_reset()
    test_protocol.start()

    # Test if success results are handled
    res_succ = ResEntanglementSuccess(remote_node_id=100, remote_node_name="remote_node", mem_pos=0,
                                      bell_index=BellIndex(3))
    entanglement_service.send_response(res_succ)
    ns.sim_run()

    # Test if fail results are handled
    res_fail = ResEntanglementFail(remote_node_name="remote_node", mem_pos=0)
    entanglement_service.send_response(res_fail)
    ns.sim_run()

    # Test if state-ready results are handled
    res_state = ResStateReady(remote_node_name="remote_node", mem_pos=0)
    entanglement_service.send_response(res_state)
    ns.sim_run()

    # Test if error results are handled
    res_error = ResEntanglementError(remote_node_name="remote_node", mem_pos=0, error_code=3)
    entanglement_service.send_response(res_error)

    assert not test_protocol.finished
    ns.sim_run()
    assert test_protocol.finished
