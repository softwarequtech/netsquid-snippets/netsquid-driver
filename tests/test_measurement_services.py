import netsquid as ns
from netsquid.nodes.node import Node
from netsquid.protocols.nodeprotocols import NodeProtocol
from netsquid_driver.measurement_services import (
    MeasureService,
    ReqMeasure,
    ReqSwap,
    ResMeasure,
    ResSwap,
    SwapService,
)


class SimpleMeasure(MeasureService):
    """Implementation of :class:`operation_services.MeasureService` meant to test request and response handling."""

    def __init__(self, node):
        super().__init__(node=node, name="simple_measure")
        self.performed_measure = False

    def measure(self, req):
        super().measure(req)
        self.performed_measure = True


class SimpleSwap(SwapService):
    """Implementation of :class:`operation_services.SwapService` meant to test request and response handling."""

    def __init__(self, node):
        super().__init__(node=node, name="simple_swap")
        self.performed_swap = False

    def swap(self, req):
        super().swap(req)
        self.performed_swap = True


class MeasureTestProtocol(NodeProtocol):
    """Protocol to test responses from measure service.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node` or None, optional
        Node this protocol runs on. If None, a node should be set later before starting
        this protocol.
    measure_service : :class:`operation_services.MeasureService`.
        Service to be tested for responses.
    name : str or None, optional
        Name of protocol. If None, the name of the class is used.

    """
    def __init__(self, node, measure_service, name="measure_test_protocol"):
        super().__init__(node=node, name=name)
        self.measure_service = measure_service
        self.finished = False

    def run(self):

        signal_res_measure = ResMeasure.__name__

        yield self.await_signal(sender=self.measure_service, signal_label=signal_res_measure)
        res_measure = self.measure_service.get_signal_result(signal_res_measure)
        assert isinstance(res_measure, ResMeasure)

        self.finished = True


class SwapTestProtocol(NodeProtocol):
    """Protocol to test responses from measure service.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node` or None, optional
        Node this protocol runs on. If None, a node should be set later before starting
        this protocol.
    swap_service : :class:`operation_services.SwapService`.
        Service to be tested for responses.
    name : str or None, optional
        Name of protocol. If None, the name of the class is used.

    """

    def __init__(self, node, swap_service, name="measure_test_protocol"):
        super().__init__(node=node, name=name)
        self.swap_service = swap_service
        self.finished = False

    def run(self):
        signal_res_swap = ResSwap.__name__

        yield self.await_signal(sender=self.swap_service, signal_label=signal_res_swap)
        res_swap = self.swap_service.get_signal_result(signal_res_swap)
        assert isinstance(res_swap, ResSwap)

        self.finished = True


def test_measure_service():
    """Test if requests and responses are handled correctly by :class:`operation_services.MeasureService`."""

    node = Node("test_measure_service_node")
    measure_service = SimpleMeasure(node)
    test_protocol = MeasureTestProtocol(node=node, measure_service=measure_service)

    # test if measure request is handled correctly
    req = ReqMeasure(mem_pos=0, x_rotation_angle_1=0, y_rotation_angle=1, x_rotation_angle_2=0)
    measure_service.put(req)
    assert measure_service.performed_measure

    # test if measure response is handled correctly
    ns.sim_reset()
    test_protocol.start()
    res = ResMeasure(outcome=None)
    measure_service.send_response(res)

    assert not test_protocol.finished
    ns.sim_run()
    assert test_protocol.finished


def test_swap_service():
    """Test if requests and responses are handled correctly by :class:`operation_services.SwapService`."""

    node = Node("test_swap_service_node")
    swap_service = SimpleSwap(node)
    test_protocol = SwapTestProtocol(node=node, swap_service=swap_service)

    # test if measure request is handled correctly
    req = ReqSwap(mem_pos_1=1, mem_pos_2=2)
    swap_service.put(req)
    assert swap_service.performed_swap

    # test if measure response is handled correctly
    ns.sim_reset()
    test_protocol.start()
    res = ResSwap(outcome=None)
    swap_service.send_response(res)

    assert not test_protocol.finished
    ns.sim_run()
    assert test_protocol.finished
