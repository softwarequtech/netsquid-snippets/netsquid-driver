from abc import ABCMeta, abstractmethod

from netsquid.nodes import Node
from netsquid.protocols import ServiceProtocol
from netsquid_driver.driver import Driver


class MockServiceABC(ServiceProtocol, metaclass=ABCMeta):
    def __init__(self, node, name="MockService"):
        super().__init__(node=node, name=name)

    @abstractmethod
    def mock_method(self):
        pass


class MockServiceInstantiation(MockServiceABC):
    def __init__(self, node, name="MockService"):
        super().__init__(node=node, name=name)

    def mock_method(self):
        pass


def test_add_service():
    driver = Driver(name="test_driver")
    node = Node(name="test_node")
    node.add_subcomponent(driver)
    test_service = MockServiceInstantiation(node)
    driver.add_service(MockServiceABC, test_service)

    assert driver.is_connected
    assert driver[MockServiceABC] is test_service
    assert driver.services == {MockServiceABC: test_service}
